/*//////////////////////////////////////////////////////////////////////////////


		██╗    ██╗██╗███╗   ██╗███╗   ███╗██╗███╗   ██╗███████╗
		██║    ██║██║████╗  ██║████╗ ████║██║████╗  ██║██╔════╝
		██║ █╗ ██║██║██╔██╗ ██║██╔████╔██║██║██╔██╗ ██║█████╗  
		██║███╗██║██║██║╚██╗██║██║╚██╔╝██║██║██║╚██╗██║██╔══╝  
		╚███╔███╔╝██║██║ ╚████║██║ ╚═╝ ██║██║██║ ╚████║███████╗
		 ╚══╝╚══╝ ╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝╚═╝╚═╝  ╚═══╝╚══════╝
                                                           
					██╗  ██╗ █████╗  ██████╗██╗  ██╗           
					██║  ██║██╔══██╗██╔════╝██║ ██╔╝           
					███████║███████║██║     █████╔╝            
					██╔══██║██╔══██║██║     ██╔═██╗            
					██║  ██║██║  ██║╚██████╗██║  ██╗           
					╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝		


//////////////////////////////////////////////////////////////////////////////*/

#include <Windows.h>
#include <stdio.h>
//#include <stdint.h>
#pragma comment( lib, "psapi" )
#include <Psapi.h>
#include "../Vendor/Detours15e/detours.h"
#pragma comment (lib, "../Libs/detours.lib")
//#include "resource.h"
#include "../Vendor/Winmm/winmm.hpp"

#pragma comment(linker, \
  "\"/manifestdependency:type='Win32' "\
  "name='Microsoft.Windows.Common-Controls' "\
  "version='6.0.0.0' "\
  "processorArchitecture='*' "\
  "publicKeyToken='6595b64144ccf1df' "\
  "language='*'\"")

typedef unsigned char       uint8_t;
typedef unsigned short      uint16_t;
typedef unsigned __int32    uint32_t;
typedef signed char         int8_t;
typedef signed short        int16_t;
typedef __int32				int32_t;

///////////////////////////////////////////////////////////////////////////////

char string[1024];
static WNDPROC		orig_wndproc;
static HWND			orig_wnd;
int x, y;
HMENU hMenu;
HMODULE g_hModule;
BOOL Cheat_AlertOnBombs = false;

typedef int(__stdcall *D_ShowBombs)(char);
typedef int(__stdcall *D_ClickBox)(unsigned int, unsigned int);
typedef int(__stdcall *D_DisplayScreen)(HDC hdc);

int __stdcall hkStepSquare(int unk1, int unk2);
typedef int(__stdcall* StepSquare_t)(int unk1, int unk2);
StepSquare_t OrigStepSquare;

////////////////////////////////////////////////////////////////////////////////

bool CheckMine(unsigned int x, unsigned int y)
{
	y = (y << 5); 
	unsigned int* UnderBox = (unsigned int*)(0x1005340 + y + x); 
	if ((BYTE)*UnderBox == 0x0F) 
		return false; 
	return true;
}

void SolveBox() 
{
	D_ClickBox ClickBox = (D_ClickBox)0x1003512;
	for (unsigned int w = 1; w <= x; w++) { 
		for (unsigned int h = 1; h <= y; h++){ 
			if (CheckMine(w, h) == false) 
				ClickBox(w, h); 
		} 
	}
}

static LRESULT CALLBACK wnd_proc(HWND wnd, UINT umsg, WPARAM wparam, LPARAM lparam)
{
	
	switch (umsg)
	{
		case WM_COMMAND:
		{
			D_ShowBombs ShowBombs = (D_ShowBombs)0x1002F80;
			if (wparam == 700) ShowBombs(10);
			if (wparam == 701) { ShowBombs(-1); InvalidateRect(*(HWND*)0x1005B24, NULL, TRUE); }
			if (wparam == 702) SolveBox();
			if (wparam == 703) { Cheat_AlertOnBombs ^= 1; if (Cheat_AlertOnBombs) { CheckMenuItem(hMenu, 703, MF_CHECKED); } else { CheckMenuItem(hMenu, 703, MF_UNCHECKED); } }
		}
		break;
	}
	return CallWindowProc(orig_wndproc, wnd, umsg, wparam, lparam);
}

#pragma region WNDPROC

void wndproc_uninstall(void)
{
	if (orig_wnd != NULL)
	{
		SetWindowLong(orig_wnd, GWL_WNDPROC, (LONG)(UINT_PTR)orig_wndproc);
		orig_wnd = NULL;
	}
}
void wndproc_install(HWND wnd)
{
	if (orig_wndproc == NULL || wnd != orig_wnd)
	{
		wndproc_uninstall();
		orig_wndproc = (WNDPROC)(UINT_PTR)SetWindowLong(wnd, GWL_WNDPROC, (LONG)(UINT_PTR)wnd_proc);
		orig_wnd = wnd;
	}
}

#pragma endregion

uint8_t	GetBox(int x, int y) { 
	return  *(uint8_t*)(0x1005340 + (x + 32 * y)); 
}

void	SetBox(int x, int y, uint8_t b)	{ 
	*(uint8_t*)(0x1005340 + (x + 32 * y)) = b; 
}

int __stdcall myStepSquare(int xX, int yY)
{
	uint8_t box = *(uint8_t*)(0x1005340 + (xX + 32 * yY));
	if (box == 128 && Cheat_AlertOnBombs)
	{
		sprintf(string, "U goin to get rekt, k?");
		if (MessageBox(NULL, string, "", MB_OKCANCEL) == 2) return 1;
	}
	return OrigStepSquare(xX, yY);
}


DWORD Thread_Hook() 
{
	while (!IsWindow(*(HWND*)0x1005B24)) {}
	wndproc_install(*(HWND*)0x1005B24);
	
	hMenu = CreateMenu();
	AppendMenu(*(HMENU*)0x1005A94, MF_POPUP, (UINT_PTR)hMenu, "Hack");
	AppendMenu(hMenu, MF_STRING, 700, "Show Bombs");
	AppendMenu(hMenu, MF_STRING, 701, "Make Bombs Unclickable");
	AppendMenu(hMenu, MF_STRING, 702, "Solve this shit");
	AppendMenu(hMenu, MF_STRING, 703, "Alert on bombs");

	while (true) {

		x = *(int*)0x1005334;
		y = *(int*)0x1005338;

		Sleep(100);
	}
	return 0;
}

BOOL APIENTRY DllMain( HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	g_hModule = hModule;
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	{
		HookWINMMLib();
		OrigStepSquare = (StepSquare_t)DetourFunction((BYTE*)(0x1003512), (BYTE*)myStepSquare);
		CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)Thread_Hook, NULL, NULL, NULL);
	}
	break;
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

