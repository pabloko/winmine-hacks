//========================================================================================
/*[[[======================================================================================


	██╗    ██╗██╗███╗   ██╗███╗   ███╗██╗███╗   ██╗███████╗   ███████╗██╗   ██╗██████╗ 
	██║    ██║██║████╗  ██║████╗ ████║██║████╗  ██║██╔════╝   ██╔════╝██║   ██║██╔══██╗
	██║ █╗ ██║██║██╔██╗ ██║██╔████╔██║██║██╔██╗ ██║█████╗     ███████╗██║   ██║██████╔╝
	██║███╗██║██║██║╚██╗██║██║╚██╔╝██║██║██║╚██╗██║██╔══╝     ╚════██║╚██╗ ██╔╝██╔══██╗
	╚███╔███╔╝██║██║ ╚████║██║ ╚═╝ ██║██║██║ ╚████║███████╗██╗███████║ ╚████╔╝ ██║  ██║
	 ╚══╝╚══╝ ╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝╚═╝╚═╝  ╚═══╝╚══════╝╚═╝╚══════╝  ╚═══╝  ╚═╝  ╚═╝
																					   
							╔╗ ╦ ╦  ╔═╗╔═╗╔╗ ╦  ╔═╗╦╔═╔═╗
							╠╩╗╚╦╝  ╠═╝╠═╣╠╩╗║  ║ ║╠╩╗║ ║
							╚═╝ ╩   ╩  ╩ ╩╚═╝╩═╝╚═╝╩ ╩╚═╝

========================================================================================*/
//========================================================================================

#include <stdio.h>
#include <tchar.h>
#pragma comment (lib, "ws2_32.lib")
#pragma comment (lib, "../Libs/lua51.lib")
#pragma comment(linker, "/NODEFAULTLIB:libcmt.lib")
#include <string.h>
#include <conio.h>
#include <stdio.h>
#include <time.h>
#include "../Vendor/RakNet4/RakPeerInterface.h"
#include "../Vendor/RakNet4/MessageIdentifiers.h"
#include "../Vendor/RakNet4/BitStream.h"
#include "../Vendor/RakNet4/RakNetTypes.h"
#include "../Vendor/LuaJIT/src/lua.hpp"

#pragma comment(linker, \
  "\"/manifestdependency:type='Win32' "\
  "name='Microsoft.Windows.Common-Controls' "\
  "version='6.0.0.0' "\
  "processorArchitecture='*' "\
  "publicKeyToken='6595b64144ccf1df' "\
  "language='*'\"")

#define SERVER_PORT 1337
#define MAX_CLIENTS 500

lua_State *L;
RakNet::RakPeerInterface *peer;
FILE *f;
char szCmd[256];

//Types over network
enum NETWORK_TYPES {
	TBOOL = 0,
	TUINT8,
	TINT8,
	TUINT16,
	TINT16,
	TUINT32,
	TINT32,
	TFLOAT,
	TDOUBLE,
	TLONG,
	TSTRING
};

//Startup server accepting connections
void StartupNetwork() {
	peer = RakNet::RakPeerInterface::GetInstance();
	RakNet::SocketDescriptor sd(SERVER_PORT, 0);
	peer->Startup(MAX_CLIENTS, &sd, 1);
	peer->SetMaximumIncomingConnections(MAX_CLIENTS);
	// szKey = "0u5S21"
	char szKey[7] = { 0x30, 0x74, 0x37, 0x50, 0x36, 0x34, 0x06 };
	for (unsigned int fsBrH = 0, tzyGC = 0; fsBrH < 7; fsBrH++)
	{
		tzyGC = szKey[fsBrH];
		tzyGC ^= fsBrH;
		szKey[fsBrH] = tzyGC;
	}
	peer->SetIncomingPassword(szKey, 6);
	peer->SetUnreliableTimeout(500);
	peer->SetOccasionalPing(true);
}

//Log a string to stdout and file
static int Log(lua_State* ls)
{
	const char * str = lua_tostring(L, 1);
	printf("%s\n", str);
	fprintf(f, "%s\n", str);
	return 0;
}

//Send a packet to some player
static int NetSend(lua_State* L)
{
	RakNet::BitStream *stream = static_cast<RakNet::BitStream*>(lua_touserdata(L, 1));
	peer->Send(stream, HIGH_PRIORITY, RELIABLE_ORDERED, 0, peer->GetSystemAddressFromIndex(lua_tonumber(L, 2) - 1), false);
	return 0;
}

//Create packet with packet id
static int NetCreateBitStream(lua_State* L)
{
	RakNet::BitStream *stream = new RakNet::BitStream();
	int v1 = luaL_checknumber(L, 1);
	stream->Write((RakNet::MessageID)v1);
	lua_pushlightuserdata(L, stream);
	return 1;
}

//Write to a bitstream
static int NetWriteBitStream(lua_State* L)
{
	RakNet::BitStream *stream = static_cast<RakNet::BitStream*>(lua_touserdata(L, 1));
	int type = lua_tointeger(L, 3);
	switch (type)
	{
	case NETWORK_TYPES::TBOOL: {
		bool var;
		var = lua_toboolean(L, 2);
		stream->Write(var);
		} break;
	case NETWORK_TYPES::TUINT8: {
		uint8_t var;
		var = luaL_checknumber(L, 2);
		stream->Write(var);
		} break;
	case NETWORK_TYPES::TINT8: {
		int8_t var;
		var = luaL_checknumber(L, 2);
		stream->Write(var);
		} break;
	case NETWORK_TYPES::TUINT16: {
		uint16_t var;
		var = luaL_checknumber(L, 2);
		stream->Write(var);
		} break;
	case NETWORK_TYPES::TINT16: {
		int16_t var;
		var = luaL_checknumber(L, 2);
		stream->Write(var);
		} break;
	case NETWORK_TYPES::TUINT32: {
		uint32_t var;
		var = luaL_checknumber(L, 2);
		stream->Write(var);
		} break;
	case NETWORK_TYPES::TINT32: {
		int32_t var;
		var = luaL_checknumber(L, 2);
		stream->Write(var);
		} break;
	case NETWORK_TYPES::TFLOAT: {
		FLOAT var;
		var = luaL_checknumber(L, 2);
		stream->Write(var);
		} break;
	case NETWORK_TYPES::TDOUBLE: {
		double var;
		var = luaL_checknumber(L, 2);
		stream->Write(var);
		} break;
	case NETWORK_TYPES::TLONG: {
		LONG var;
		var = luaL_checklong(L, 2);
		stream->Write(var);
		} break;
	case NETWORK_TYPES::TSTRING: {
		const char* vStr;
		vStr = luaL_checkstring(L, 2);
		RakNet::RakString str = vStr;
		stream->Write(str);
		} break;
	}
	return 0;
}

//read from a bitstream
static int NetReadBitStream(lua_State* L)
{
	RakNet::BitStream *stream = static_cast<RakNet::BitStream*>(lua_touserdata(L, 1));
	int type = lua_tointeger(L, 2);
	switch (type)
	{
	case NETWORK_TYPES::TBOOL: {
		bool var;
		stream->Read(var);
		lua_pushboolean(L, var);
		} break;
	case NETWORK_TYPES::TUINT8: {
		uint8_t var;
		stream->Read(var);
		lua_pushnumber(L, var);
		} break;
	case NETWORK_TYPES::TINT8: {
		int8_t var;
		stream->Read(var);
		lua_pushnumber(L, var);
		} break;
	case NETWORK_TYPES::TUINT16: {
		uint16_t var;
		stream->Read(var);
		lua_pushnumber(L, var);
		} break;
	case NETWORK_TYPES::TINT16: {
		int16_t var;
		stream->Read(var);
		lua_pushnumber(L, var);
		} break;
	case NETWORK_TYPES::TUINT32: {
		uint32_t var;
		stream->Read(var);
		lua_pushnumber(L, var);
		} break;
	case NETWORK_TYPES::TINT32: {
		int32_t var;
		stream->Read(var);
		lua_pushnumber(L, var);
		} break;
	case NETWORK_TYPES::TFLOAT: {
		FLOAT var;
		stream->Read(var);
		lua_pushnumber(L, var);
		} break;
	case NETWORK_TYPES::TDOUBLE: {
		double var;
		stream->Read(var);
		lua_pushnumber(L, var);
		} break;
	case NETWORK_TYPES::TLONG: {
		LONG var;
		stream->Read(var);
		lua_pushnumber(L, var);
		} break;
	case NETWORK_TYPES::TSTRING: {
		RakNet::RakString str;
		stream->Read(str);
		lua_pushstring(L, str.C_String());
		} break;
	}
	return 1;
}

//kick a faggot
static int Kick(lua_State* L)
{
	int client = luaL_checknumber(L, 1);
	peer->CloseConnection(peer->GetSystemAddressFromIndex(client-1), true);
	return 0;
}

//banhammer them all
static int Ban(lua_State* L)
{
	int client = luaL_checknumber(L, 1);
	int time = luaL_checknumber(L, 2) * 1000;
	peer->AddToBanList(peer->GetSystemAddressFromIndex(client).ToString(false), time);
	peer->CloseConnection(peer->GetSystemAddressFromIndex(client-1), true);
	return 0;
}

//This can help
static int lGetTickCount(lua_State* L)
{
	lua_pushnumber(L, GetTickCount());
	return 1;
}

//This can help too
static int lSleep(lua_State* L)
{
	Sleep(lua_tonumber(L,1));
	return 0;
}

//Thread fagget system//

//struct definition for thread calls
struct stOnArgs {
	int pFn;          //This will hold lua->c function ptr by magic
	int iRepeat;      //Repeat beh. -1 = infinite, >0 will repeat these times
	int iMsDelay;     //Delay between ticks
};

//Function to be threaded
int ThreadOn(void* arg) {
	stOnArgs* stArgs = (stOnArgs*)arg;
	do {
		Sleep(stArgs->iMsDelay);
		lua_rawgeti(L, LUA_REGISTRYINDEX, stArgs->pFn);
		if (lua_isfunction(L, -1))
		{
			if (lua_pcall(L, 0, 0, 0) != 0)
				printf("%s\n", lua_tostring(L, -1));
		}
		else
			lua_remove(L, -1);
		if (stArgs->iRepeat > 0)
			stArgs->iRepeat--;
	} while (stArgs->iRepeat != 0);
	return 0;
}

//Start a thread
static int lOn(lua_State* L)
{
	stOnArgs* stArgs = new stOnArgs();
	stArgs->iRepeat = luaL_checknumber(L, 1);
	stArgs->iMsDelay = luaL_checknumber(L, 2);
	stArgs->pFn = luaL_ref(L, LUA_REGISTRYINDEX);
	HANDLE pThread = CreateThread(0, 0, (LPTHREAD_START_ROUTINE)ThreadOn, stArgs, 0, 0);
	lua_pushlightuserdata(L, pThread);
	return 1;
}

//Cancel a thread
static int lCancel(lua_State* L)
{
	HANDLE pThread = static_cast<HANDLE>(lua_touserdata(L, 1));
	TerminateThread(pThread, 0);
	return 0;
}

//Alternative and simpler thread to run a default "main"
int LuaThread(void) {
	while (1) {
		lua_getglobal(L, "main");
		if (lua_isfunction(L, -1))
		{
			if (lua_pcall(L, 0, 0, 0) != 0) {
				printf("%s\n", lua_tostring(L, -1));
				return 0;
			}
		}
		else {
			lua_remove(L, -1);
			return 0;
		}
	}
	return 0;
}

//Load script file
void ExecuteServerRoutine() {
	if (luaL_dofile(L, "script.lua"))
		printf("%s\n", lua_tostring(L, -1));
}

//Entrypoint of this APP
int _tmain(int argc, _TCHAR* argv[])
{
	//Start random number generator
	srand(time(NULL));
	RakNet::Packet *packet;
	//Initialize network
	StartupNetwork();
	//Prepare file to store logs
	f = fopen("Server.log", "a");
	//Initialice LUA
	L = lua_open();
	luaL_openlibs(L);
	//Define TYPES on LUA
	lua_pushnumber(L, NETWORK_TYPES::TBOOL);
	lua_setglobal(L, "TBOOL");
	lua_pushnumber(L, NETWORK_TYPES::TUINT8);
	lua_setglobal(L, "TUINT8");
	lua_pushnumber(L, NETWORK_TYPES::TINT8);
	lua_setglobal(L, "TINT8");
	lua_pushnumber(L, NETWORK_TYPES::TUINT16);
	lua_setglobal(L, "TUINT16");
	lua_pushnumber(L, NETWORK_TYPES::TINT16);
	lua_setglobal(L, "TINT16");
	lua_pushnumber(L, NETWORK_TYPES::TUINT32);
	lua_setglobal(L, "TUINT32");
	lua_pushnumber(L, NETWORK_TYPES::TINT32);
	lua_setglobal(L, "TINT32");
	lua_pushnumber(L, NETWORK_TYPES::TFLOAT);
	lua_setglobal(L, "TFLOAT");
	lua_pushnumber(L, NETWORK_TYPES::TDOUBLE);
	lua_setglobal(L, "TDOUBLE");
	lua_pushnumber(L, NETWORK_TYPES::TLONG);
	lua_setglobal(L, "TLONG");
	lua_pushnumber(L, NETWORK_TYPES::TSTRING);
	lua_setglobal(L, "TSTRING");
	//Define network function holder
	lua_newtable(L);
	lua_setglobal(L, "NetProcess");
	//Register all the functions
	lua_register(L, "Log", Log);
	lua_register(L, "NetSend", NetSend);
	lua_register(L, "NetCreateBitStream", NetCreateBitStream);
	lua_register(L, "NetWriteBitStream", NetWriteBitStream);
	lua_register(L, "NetReadBitStream", NetReadBitStream);
	lua_register(L, "Kick", Kick);
	lua_register(L, "Ban", Ban);
	lua_register(L, "GetTickCount", lGetTickCount);
	lua_register(L, "Sleep", lSleep);
	lua_register(L, "On", lOn);
	lua_register(L, "Cancel", lCancel);
	sprintf(szCmd, "");
	//Run the script.lua file
	ExecuteServerRoutine();
	//Start the "main" thread
	HANDLE ThreadHandle = CreateThread(0, 0, (LPTHREAD_START_ROUTINE)LuaThread, 0, 0, 0);
	//App lifecycle
	while (1)
	{
		//Fuck bitches, get packets
		for (packet = peer->Receive(); packet; peer->DeallocatePacket(packet), packet = peer->Receive()) {
			lua_getglobal(L, "NetProcess");
			lua_pushnumber(L, packet->data[0]);
			lua_gettable(L, -2);
			lua_remove(L, -2);
			if (lua_isfunction(L, -1))
			{
				lua_pushinteger(L, packet->systemAddress.systemIndex+1);
				RakNet::BitStream stream(packet->data, packet->length, false);
				stream.IgnoreBytes(sizeof(RakNet::MessageID));
				lua_pushlightuserdata(L, &stream);
				if (lua_pcall(L, 2, 0, 0) != 0)
					printf("%s\n", lua_tostring(L, -1));
			}
			else
				lua_remove(L, -1);
		}

		//Reload F11+F12
		if (GetAsyncKeyState(VK_F12) && GetAsyncKeyState(VK_F11))
		{
			TerminateThread(ThreadHandle, 0);
			peer->Shutdown(100);
			RakNet::RakPeerInterface::DestroyInstance(peer);
			StartupNetwork();
			ExecuteServerRoutine();
			ThreadHandle = CreateThread(0, 0, (LPTHREAD_START_ROUTINE)LuaThread, 0, 0, 0);
			Sleep(500);
		}

		//Input
		int ch;
		while (_kbhit())
		{
			ch = _getch();
			if (ch == 0x0D) {
				printf("\n");
				lua_getglobal(L, "onCmd");
				if (lua_isfunction(L, -1))
				{
					lua_pushstring(L, szCmd);
					if (lua_pcall(L, 1, 0, 0) != 0)
						printf("%s\n", lua_tostring(L, -1));
				}
				else
					lua_remove(L, -1);
				sprintf(szCmd, "");
			}
			else {
				printf("%c", ch);
				sprintf(szCmd, "%s%c", szCmd, ch);
			}
		}
	}
	//End with the pain
	fclose(f);
	peer->Shutdown(100);
	RakNet::RakPeerInterface::DestroyInstance(peer);
	return 0;
}
