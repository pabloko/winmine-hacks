/*/////////////////////////////////////////////////////////////////////////////////////



	███╗   ███╗ ██████╗ ██████╗       ███████╗██╗   ██╗██████╗  ██████╗ ██╗  ██╗██╗   ██╗
	████╗ ████║██╔═══██╗██╔══██╗      ██╔════╝██║   ██║██╔══██╗██╔═══██╗██║ ██╔╝██║   ██║
	██╔████╔██║██║   ██║██║  ██║█████╗███████╗██║   ██║██║  ██║██║   ██║█████╔╝ ██║   ██║
	██║╚██╔╝██║██║   ██║██║  ██║╚════╝╚════██║██║   ██║██║  ██║██║   ██║██╔═██╗ ██║   ██║
	██║ ╚═╝ ██║╚██████╔╝██████╔╝      ███████║╚██████╔╝██████╔╝╚██████╔╝██║  ██╗╚██████╔╝
	╚═╝     ╚═╝ ╚═════╝ ╚═════╝       ╚══════╝ ╚═════╝ ╚═════╝  ╚═════╝ ╚═╝  ╚═╝ ╚═════╝
				╔═╗╔═╗╦═╗  ╦ ╦╦╔╗╔╔╦╗╦╔╗╔╔═╗  ╔╗ ╦ ╦  ╔═╗╔═╗╔╗ ╦  ╔═╗╦╔═╔═╗
				╠╣ ║ ║╠╦╝  ║║║║║║║║║║║║║║║╣   ╠╩╗╚╦╝  ╠═╝╠═╣╠╩╗║  ║ ║╠╩╗║ ║
				╚  ╚═╝╩╚═  ╚╩╝╩╝╚╝╩ ╩╩╝╚╝╚═╝  ╚═╝ ╩   ╩  ╩ ╩╚═╝╩═╝╚═╝╩ ╩╚═╝


/////////////////////////////////////////////////////////////////////////////////////*/
#define WIN32_LEAN_AND_MEAN 
// Is build with hacks? (comment for release)
#define HACK_BUILD

//Includes frst
#include <windows.h>
#include <stdio.h>
//#include <stdint.h>
#include <Psapi.h>
#include "../Vendor/Detours15e/detours.h"
#include "../Vendor/Winmm/winmm.hpp"
#include <string.h>
#include "resource.h"
#include <time.h>
#include <CommCtrl.h>
#include <tchar.h>
#include <Shellapi.h>

//Add some libs
#pragma comment (lib, "psapi" )
#pragma comment (lib, "../Libs/detours.lib")
#pragma comment (lib, "Shell32.lib" )
#pragma comment (lib, "gdiplus.lib" )
#pragma comment (lib, "ComCtl32.lib")

//Include a common controls manifest
#pragma comment(linker, \
  "\"/manifestdependency:type='Win32' "\
  "name='Microsoft.Windows.Common-Controls' "\
  "version='6.0.0.0' "\
  "processorArchitecture='*' "\
  "publicKeyToken='6595b64144ccf1df' "\
  "language='*'\"")

typedef unsigned char       uint8_t;
typedef unsigned short      uint16_t;
typedef unsigned __int32    uint32_t;
typedef signed char         int8_t;
typedef signed short        int16_t;
typedef __int32				int32_t;

//Cursor movement defines
#define ORIENTATION_VERTICAL 0
#define ORIENTATION_HORIZONTAL 1
#define DIRECTION_FORWARD 0
#define DIRECTION_BACKWARD 1

// Drawing constants
#define CELL_SIZE 16
#define HEADER_MARGIN 54
#define LEFT_MARGIN 11

// Button faces
#define FACE_IDLE	0
#define FACE_WOT	1
#define FACE_REKT	2
#define FACE_WIN	3

/* WINMINE'S ADDRS AT VERSION 5.1.2600.0
Language:			1033/1200
CompanyName:        Microsoft Corporation
FileDescription:    Entertainment Pack Minesweeper Game
FileVersion:        5.1.2600.0 (xpclient.010817-1148)
InternalName:       winmine
OriginalFilename:   WINMINE.EXE
ProductName:        Microsoft® Windows® Operating System
ProductVersion:     5.1.2600.0 */
#define ADDR_MAINHWND	*(HWND*)	0x1005B24
#define ADDR_HMENU		*(HMENU*)	0x1005A94
#define ADDR_LEFTCOUNT	*(int*)		0x1005194
#define ADDR_RAWMATRIX				0x1005340
#define ADDR_DISPLAYBLK				0x1002646
#define ADDR_ISCOLOR	*(bool*)	0x10056C8
#define ADDR_BLKBITMAP	*(HGLOBAL*)	0x1005958
#define ADDR_EAUDIO		*(int*)		0x10056B8
#define ADDR_GRIDX1		*(int*)		0x1005334
#define ADDR_GRIDX2		*(int*)		0x1005338
#define ADDR_GRIDY1		*(int*)		0x10056A8
#define ADDR_GRIDY2		*(int*)		0x10056AC
#define ADDR_DISPLAYLC				0x1002801
#define ADDR_DRAWBTN				0x1002913
#define ADDR_STEPSQUARE				0x1003512
#define ADDR_FINDBITMAP				0x10023CD
#define ADDR_STARTGAME				0x100367A
#define ADDR_RPREFS					0x1002BC2

//lets use QQwing Sudoku logic
#include "../Vendor/QQwing/qqwing.hpp"
using namespace qqwing;

//our static definitions
//Cursor
int					iCursorX = 1;
int					iCursorY = 1;
int					uCursorTicks = 0;
bool				bCurToggled = false;
uint8_t				uCurBox = 0;
//Key handling
int					uKeysTicks = 0;
//Game
int					iDificulty = 0;
bool				bGameEnded = true;
bool				bGamePaused = false;
bool				bHasFocus = true;
//Sudoku related
const int*			puzzle;
const int*			solution;
//Game commands
bool				bHint = false;
bool				bCheck = false;
//Game matrix
bool				bGridInitial[9][9];
uint8_t				bGridBackup[9][9];
uint8_t				bGridSolved[9][9];
//Default hook
HANDLE				g_GameMainHandle;
HMODULE				g_hModule;
static WNDPROC		orig_wndproc;
static HWND			orig_wnd;
//Helpers
long long			redirectBackup[100];
char				sstring[1024];
int					x, y;
//Brushes
HBRUSH hRed =		CreateSolidBrush(RGB(255, 0, 0));
HBRUSH hBlue =		CreateSolidBrush(RGB(0, 0, 255));
HBRUSH hGray =		CreateSolidBrush(RGB(0xcc, 0xcc, 0xcc));

// Our protorypes
void		DrawGameOverlay();
uint8_t		GetBox(int x, int y);
void		SetBox(int x, int y, uint8_t b, bool bDisplay = true);
LRESULT CALLBACK GameWndProc(HWND wnd, UINT umsg, WPARAM wparam, LPARAM lparam);
void		CheckSudokuKey(int key, uint8_t value);
int			UpdateGameCounter();
void		CheckGameFinished(int num);
void		ProcessGameCommands();
void		ProcessMoveCursor(int vk_key, bool orientation, bool direction);
DWORD		GameMainThread();
void		HookWINMMLib();
void		DrawButton(int btn);
void		ProcessMouseWheel(short delta);
void		ToggleGameState();
void		ChangeDifficulty(int lvl);
void		FixMouseMoveBugs();

//type definitions for direct calls to winmine fn's
typedef int(__stdcall *D_ShowBombs)(char);
typedef int(__stdcall *D_ClickBox)(unsigned int, unsigned int);
typedef int(__stdcall *D_DisplayScreen)(HDC hdc);
typedef int(__stdcall *D_DisplayBlk)(int, int);
typedef void(__stdcall*DrawButton_t)(int);
typedef int(__stdcall* DisplayBombCount_t)();

//Definitions for detoured winmine fn's
int __stdcall hkStepSquare(int unk1, int unk2);
typedef int(__stdcall* StepSquare_t)(int unk1, int unk2);
StepSquare_t OrigStepSquare;
int __stdcall hkClearField();
typedef int(__stdcall *ClearBox_t)();
ClearBox_t OrigClearBox;
HRSRC __stdcall hkFindBitmap(__int16 unk1);
typedef HRSRC(__stdcall* FindBitmap_t)(__int16 unk1);
FindBitmap_t OrigFindBitmap;
int __cdecl hkStartGame();
typedef int(__cdecl* StartGame_t)();
StartGame_t OrigStartGame;
int __stdcall hkReadPreferences();
typedef int(__stdcall* ReadPreferences_t)();
ReadPreferences_t OrigReadPreferences;

//Redirection helpers
#pragma region REDIRECTIONHELPER
void Redirect(void* source, void* destination, int slot) {
	DWORD dwOld;
	VirtualProtect(source, 5, PAGE_EXECUTE_READWRITE, &dwOld);
	redirectBackup[slot] = *(long long*)source;
	*((unsigned char*)source) = 0xE9;
	*(DWORD*)(((char*)source) + 1) = (DWORD)((DWORD)destination - (DWORD)source) - 5;
}

void Restore(void* source, int slot) {
	*(long long*)source = redirectBackup[slot];
}
#pragma endregion

//wndproc helper
#pragma region WNDPROC
void wndproc_uninstall(void) {
	if (orig_wnd != NULL)
	{
		SetWindowLong(orig_wnd, GWL_WNDPROC, (LONG)(UINT_PTR)orig_wndproc);
		orig_wnd = NULL;
	}
}
void wndproc_install(HWND wnd) {
	if (orig_wndproc == NULL || wnd != orig_wnd)
	{
		wndproc_uninstall();
		orig_wndproc = (WNDPROC)(UINT_PTR)SetWindowLong(wnd, GWL_WNDPROC, (LONG)(UINT_PTR)GameWndProc);
		orig_wnd = wnd;
	}
}
#pragma endregion

//This will draw a header and sudoku grid
void DrawGameOverlay() {
	//Draw the header and prepare everithing
	RECT windowRect;
	GetClientRect(ADDR_MAINHWND, &windowRect);
	int windowX = windowRect.right;
	int windowY = windowRect.bottom;
	HDC hDC;
	hDC = GetDC(ADDR_MAINHWND);
	HFONT hfontArial;
	HGDIOBJ oldfont;
	RECT rekt = { 0, 0, windowX, 15 };
	hfontArial = CreateFontA(10, 0, FW_DONTCARE, FW_DONTCARE, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_DONTCARE, "Arial");
	oldfont = SelectObject(hDC, hfontArial);
	SetTextColor(hDC, RGB(0, 0, 0));
	DrawTextA(hDC, "MineSweeper-Sudoku MOD (Pabloko)", strlen("MineSweeper-Sudoku MOD (Pabloko)"), &rekt, DT_SINGLELINE | DT_VCENTER | DT_CENTER);
	HPEN hLinePen;
	COLORREF qLineColor;
	qLineColor = RGB(0, 0, 0);
	hLinePen = CreatePen(PS_SOLID, 2, qLineColor);
	HPEN hPenOld = (HPEN)SelectObject(hDC, hLinePen);
	//Draw sudoku grid pls
	//Verticals (starts at top-left)
	MoveToEx(hDC, LEFT_MARGIN, HEADER_MARGIN - 1, NULL);
	LineTo(hDC, LEFT_MARGIN, HEADER_MARGIN + (CELL_SIZE * 9));
	MoveToEx(hDC, LEFT_MARGIN + (CELL_SIZE * 3), HEADER_MARGIN, NULL);
	LineTo(hDC, LEFT_MARGIN + (CELL_SIZE * 3), HEADER_MARGIN + (CELL_SIZE * 9));
	MoveToEx(hDC, LEFT_MARGIN + (CELL_SIZE * 6), HEADER_MARGIN, NULL);
	LineTo(hDC, LEFT_MARGIN + (CELL_SIZE * 6), HEADER_MARGIN + (CELL_SIZE * 9));
	MoveToEx(hDC, LEFT_MARGIN + (CELL_SIZE * 9), HEADER_MARGIN, NULL);
	LineTo(hDC, LEFT_MARGIN + (CELL_SIZE * 9), HEADER_MARGIN + (CELL_SIZE * 9));
	//Horizontals
	MoveToEx(hDC, LEFT_MARGIN, HEADER_MARGIN, NULL);
	LineTo(hDC, LEFT_MARGIN + (CELL_SIZE * 9), HEADER_MARGIN);
	MoveToEx(hDC, LEFT_MARGIN, HEADER_MARGIN + (CELL_SIZE * 3), NULL);
	LineTo(hDC, LEFT_MARGIN + (CELL_SIZE * 9), HEADER_MARGIN + (CELL_SIZE * 3));
	MoveToEx(hDC, LEFT_MARGIN, HEADER_MARGIN + (CELL_SIZE * 6), NULL);
	LineTo(hDC, LEFT_MARGIN + (CELL_SIZE * 9), HEADER_MARGIN + (CELL_SIZE * 6));
	MoveToEx(hDC, LEFT_MARGIN, HEADER_MARGIN + (CELL_SIZE * 9), NULL);
	LineTo(hDC, LEFT_MARGIN + (CELL_SIZE * 9), HEADER_MARGIN + (CELL_SIZE * 9));
	//finish this shit
	SelectObject(hDC, hPenOld);
	DeleteObject(hLinePen);
	DeleteObject(hfontArial);
	ReleaseDC(ADDR_MAINHWND, hDC);
}

//obtain a winmine's blk content
uint8_t GetBox(int x, int y) {
	return *(BYTE*)(ADDR_RAWMATRIX + (x + (32 * y)));
}

//set a winmine's blk content (and display it)
void SetBox(int x, int y, uint8_t b, bool bDisplay) {
	int cx = x; int cy = y;
	y = y << 5; y = y + x;
	*(BYTE*)(ADDR_RAWMATRIX + y) = b;
	//Update blk after set it to display
	D_DisplayBlk DisplayBlk = (D_DisplayBlk)ADDR_DISPLAYBLK;
	if (bDisplay)
		DisplayBlk(cx, cy);
	DrawGameOverlay();
}

//Our hooked wndproc, winmine's msgs will arrive here first, then msgs go to defalt wndproc by default
static LRESULT CALLBACK GameWndProc(HWND wnd, UINT umsg, WPARAM wparam, LPARAM lparam) {
	bool bRet = true;
	switch (umsg) {
	// getasynckeystate will work even if window is not on top, so we need to know if window is active
	case WM_SETFOCUS:
	case WM_KILLFOCUS:
			bHasFocus = true;
		break;
	case WM_ACTIVATE:
			bHasFocus = true;
		break;
	// let the PAINT msg on default wndproc and then draw our stuff on top
	case WM_PAINT: {
		bHasFocus = true;
		CallWindowProc(orig_wndproc, wnd, umsg, wparam, lparam);
		DrawGameOverlay();
		bRet = false;
	}
	break;
	//Handle our menu items
	case WM_COMMAND: {
		switch (wparam) {
		case 801: // dificulty start
		case 802:
		case 803:
		case 804:	
			ChangeDifficulty(wparam-801);
			break; //dificulty end
		case 810: //hint
			bHint = true;
			break;
		case 811: //check
			bCheck = true;
			break;
		case 805: //showabout
			MessageBoxW(NULL, L"Made by master pabloko\npbl@0x911.ml\n\nWhat the fuck did you just fucking say about me, you little bitch? I’ll have you know I graduated top of my class in the Navy Seals, and I’ve been involved in numerous secret raids on Al-Quaeda, and I have over 300 confirmed kills. I am trained in gorilla warfare and I’m the top sniper in the entire US armed forces. You are nothing to me but just another target. I will wipe you the fuck out with precision the likes of which has never been seen before on this Earth, mark my fucking words. You think you can get away with saying that shit to me over the Internet? Think again, fucker. As we speak I am contacting my secret network of spies across the USA and your IP is being traced right now so you better prepare for the storm, maggot. The storm that wipes out the pathetic little thing you call your life. You’re fucking dead, kid. I can be anywhere, anytime, and I can kill you in over seven hundred ways, and that’s just with my bare hands. Not only am I extensively trained in unarmed combat, but I have access to the entire arsenal of the United States Marine Corps and I will use it to its full extent to wipe your miserable ass off the face of the continent, you little shit. If only you could have known what unholy retribution your little “clever” comment was about to bring down upon you, maybe you would have held your fucking tongue. But you couldn’t, you didn’t, and now you’re paying the price, you goddamn idiot. I will shit fury all over you and you will drown in it. You’re fucking dead, kiddo.", L"MineSweeper - Sudoku M0d", MB_OK);
			if (MessageBox(NULL, "Paco, España es roja.\nMátalos a todos", "Microsoft", MB_OKCANCEL) != 2) 
				ShellExecuteW(0, 0, L"https://www.youtube.com/watch?v=1L5gEXOvoEc", 0, 0, SW_SHOW);
			break;
		case 820: //Pause
			ToggleGameState();
			break;
		}
	}
	break;
	// Handle number switching with mousewheel
	case WM_MOUSEWHEEL:
	{
		//handle mousewheel
		if (!bGameEnded)
			ProcessMouseWheel(GET_WHEEL_DELTA_WPARAM(wparam));
	} break;
	case WM_CLOSE: 
	case WM_DESTROY:
		TerminateThread(g_GameMainHandle, 0); exit(0); //Kill everithing to prevent zombie process on exit
		break;
	} // end switch
	//return to minesweeper wndproc this msg (or not)
	if (bRet)
		return CallWindowProcW(orig_wndproc, wnd, umsg, wparam, lparam);
	else
		return 0;
}

// Hook on blk click
int __stdcall myStepSquare(int xX, int yY) {
	// set cursor if game is running and this cell is not default game puzzle
	if (!bGridInitial[xX][yY] && !bGameEnded && !bGamePaused)
	{
		SetBox(iCursorX, iCursorY, uCurBox); //redraw current blk to avoid cursor fixed bug
		iCursorX = xX; iCursorY = yY; uCurBox = GetBox(iCursorX, iCursorY); //update values
		SetBox(iCursorX, iCursorY, 10); //set cursor on box directly (visually nicer)
		bCurToggled = true; uCursorTicks = 2;
	}
	//redraw container boxes
	DrawGameOverlay();
	return 1;
}

// Replace blk bitmaps
HRSRC __stdcall myFindBitmap(__int16 rid) {
	if (rid == 410) { //ID 410 / 411 Contains blk bitmaps 
		bool isColor = (ADDR_ISCOLOR) ^ 1;
		HRSRC src;
		if (isColor)
			src = FindResourceW(g_hModule, MAKEINTRESOURCEW(IDB_BN_BLK), MAKEINTRESOURCEW(2));
		else
			src = FindResourceW(g_hModule, MAKEINTRESOURCEW(IDB_COLOR_BLK), MAKEINTRESOURCEW(2));
		ADDR_BLKBITMAP = LoadResource(g_hModule, src); //Setting this directly is a trick
		return NULL; //This will prevent default FindResource with old instance
	}
	return OrigFindBitmap(rid);
}

// Game start routine hooked
int __cdecl myStartGame() {
	//original fuction call
	int iRet = OrigStartGame();
	//start timer counter
	*(char*)0x1005164 = 1;
	SetTimer(ADDR_MAINHWND, 1u, 0x3E8u, 0);
	//cleanup boxes
	int dx = 1;
	int dy = 1;
	for (int i = 0; i < BOARD_SIZE; i++){
		SetBox(dx, dy, 0);
		dx++; if (dx == 10) { dx = 1; dy++; }
	}
	//Generate sudoku QQWING
	SudokuBoard::Symmetry symmetry = SudokuBoard::RANDOM;
	SudokuBoard* ss = new SudokuBoard();
	//Test dificulty
	do {
		if (ss->generatePuzzleSymmetry(symmetry)) {
			ss->solve();
		}
	} while (ss->getDifficulty() > (SudokuBoard::Difficulty)(iDificulty+1));
	//fill our data
	puzzle = ss->getPuzzle();
	solution = ss->getSolution();
	//set initial counter
	ADDR_LEFTCOUNT = ss->getGivenCount();
	//populate matrix data and boxes
	dx = 1; dy = 1; char szPuzzle[100]; sprintf(szPuzzle, "");
	bool cursorset = false;
	for (int i = 0; i < BOARD_SIZE; i++){
		//Set winmine cell
		SetBox(dx, dy, puzzle[i]);
		//Save initial state to prevent input on this blk
		if (puzzle[i]>0)
			bGridInitial[dx][dy] = true;
		else
			bGridInitial[dx][dy] = false;
		//backup data
		bGridBackup[dx][dy] = puzzle[i];
		//save solved grid matrix
		bGridSolved[dx][dy] = solution[i];
		//set the cursor on the first available spot
		if (!cursorset) 
			if (!bGridInitial[dx][dy]) { iCursorX = dx; iCursorY = dy; cursorset = true; }
		//Populate string of unsolved puzzle
		sprintf(szPuzzle, "%s%d", szPuzzle, puzzle[i]);
		dx++;if (dx == 10) {dx = 1;dy++;}
	}
	// Copy unsolved raw puzzle string to clipboard for further debug on:
	// http://qqwing.com/solve.html
	#ifdef HACK_BUILD
	if (OpenClipboard(ADDR_MAINHWND)) {
		HGLOBAL clipbuffer;
		char * buffer;
		EmptyClipboard();
		clipbuffer = GlobalAlloc(GMEM_DDESHARE, strlen(szPuzzle) + 1);
		buffer = (char*)GlobalLock(clipbuffer);
		strcpy(buffer, (LPCSTR)szPuzzle);
		GlobalUnlock(clipbuffer);
		SetClipboardData(CF_TEXT, clipbuffer);
		CloseClipboard();
	}
	#endif
	//Restore button face
	DrawButton(FACE_IDLE);
	//eneable user input
	bGameEnded = false;
	//return from original call
	return iRet;
}

// fuck preferences, force 9x9 matrix
int __stdcall myReadPreferences() {
	int iRet = OrigReadPreferences();
	//dis shit smash those values (dirty but k)
	ADDR_GRIDX1 = 9;
	ADDR_GRIDX2 = 9;
	ADDR_GRIDY1 = 9;
	ADDR_GRIDY2 = 9;
	//no audio
	ADDR_EAUDIO = 0;
	return iRet;
}

// Change face button
void DrawButton(int btn) {
	DrawButton_t hkDrawButton = (DrawButton_t)ADDR_DRAWBTN;
	hkDrawButton(btn);
}

//Sudoku helper: check sudoku number keys
void CheckSudokuKey(int key, uint8_t value) {
	if (GetAsyncKeyState(key)) {
		DrawButton(FACE_WOT);
		SetBox(iCursorX, iCursorY, value); uCurBox = value; bGridBackup[iCursorX][iCursorY] = value;
		Sleep(50); DrawButton(FACE_IDLE);
	}
}

// Update sudoku resolved count box
int UpdateGameCounter() {
	//set current counter
	int num = 0; int sx = 1; int sy = 1;
	for (int i = 0; i < BOARD_SIZE; i++) {
		//increment counter
		if (GetBox(sx, sy) > 0) num++;
		sx++; if (sx == 10) { sx = 1; sy++; }
	}
	//set and display
	ADDR_LEFTCOUNT = num;
	DisplayBombCount_t DisplayBombCount = (DisplayBombCount_t)ADDR_DISPLAYLC;
	DisplayBombCount();
	return num;
}

// End game logic
void CheckGameFinished(int num) {
	// if game is finished
	if (num == 81) { // 9x9=81 fag
		bool bWin = true;
		int sx = 1; int sy = 1;
		//check if rekt
		for (int i = 0; i < BOARD_SIZE; i++){
			if (GetBox(sx, sy) != bGridSolved[sx][sy]) {
				bWin = false;
				break;
			}
			sx++; if (sx == 10) { sx = 1; sy++; }
		}
		//draw deal with it/rekt face
		if (bWin)
			DrawButton(FACE_WIN);
		else
			DrawButton(FACE_REKT);
		//should end game to prevent more user input
		bGameEnded = true;
		KillTimer(ADDR_MAINHWND, 1u);
	}
}

// Process game commands (hint, check, etc...)
void ProcessGameCommands() {
	//HINT
	if (bHint || GetAsyncKeyState(VK_F3)) {
		DrawButton(FACE_WOT);
		//Show question blk and correct answer iteration
		int i = 0; do {
			SetBox(iCursorX, iCursorY, bGridSolved[iCursorX][iCursorY]); //Display good value
			Sleep(150);
			SetBox(iCursorX, iCursorY, 13); //Display question icon blk
			Sleep(100); i++;
		} while (i < 3);
		uCurBox = bGridSolved[iCursorX][iCursorY]; bGridBackup[iCursorX][iCursorY] = bGridSolved[iCursorX][iCursorY];
		SetBox(iCursorX, iCursorY, /*uCurBox*/bGridSolved[iCursorX][iCursorY]); //Restore this cell to correct
		bHint = false;
		DrawButton(FACE_IDLE);
	}
	//CHECK if cells are all ok
	if (bCheck || GetAsyncKeyState(VK_F4)) {
		DrawButton(FACE_WOT);
		int repeat = 10; //Iterator to show red cross blk and correct item
		do {int sx = 1; int sy = 1;
			for (int i = 0; i < BOARD_SIZE; i++) {
				//is this blk incorrect and not default?
				if (bGridBackup[sx][sy] > 0 && bGridBackup[sx][sy] != bGridSolved[sx][sy]) 
					if (repeat % 2 != 0) //is number pair?
						SetBox(sx, sy, /*bGridSolved*/bGridBackup[sx][sy]); //Restore or show correct item
					else
						SetBox(sx, sy, 11); //red cross blk
				sx++; if (sx == 10) { sx = 1; sy++; }
			}
			Sleep(150); //Sleep some time
			repeat--;
		} while (repeat > 0);
		bCheck = false;
		DrawButton(FACE_IDLE);
	}
	#ifdef HACK_BUILD
	//SOLVE hack trick f6+f8 heheheheh
	if (GetAsyncKeyState(VK_F6) && GetAsyncKeyState(VK_F8)) {
		int sx = 1; int sy = 1;
		for (int i = 0; i < BOARD_SIZE; i++){
			SetBox(sx, sy, bGridSolved[sx][sy]); // Set correct val
			sx++; if (sx == 10) { sx = 1; sy++; }
		}
		Sleep(300);
	}
	#endif
}

//Process cursor movements with arrow keys
void ProcessMoveCursor(int vk_key, bool orientation, bool direction) {
	if (GetAsyncKeyState(vk_key)) {
		DrawButton(FACE_WOT);
		SetBox(iCursorX, iCursorY, uCurBox); //Fisrt of all restore the current blk
		//Move box and repeat if encounter a default puzzle box (not editable)
		do {
			if (orientation) //		ORIENTATION_HORIZONTAL:
				if (direction) //		DIRECTION_FORWARD
					if (iCursorX == 9)
						iCursorX = 1;
					else
						iCursorX++;
				else //					DIRECTION_BACKWARD
					if (iCursorX == 1)
						iCursorX = 9;
					else
						iCursorX--;
			else //					ORIENTATION_VERTICAL:
				if (direction) //		DIRECTION_FORWARD
					if (iCursorY == 9)
						iCursorY = 1;
					else
						iCursorY++;
				else //					DIRECTION_BACKWARD
					if (iCursorY == 1)
						iCursorY = 9;
					else
						iCursorY--;
		} while (bGridInitial[iCursorX][iCursorY]); //Check defaults
		uCurBox = GetBox(iCursorX, iCursorY); //Update current box value
		SetBox(iCursorX, iCursorY, 10); //Set cursor (visually better)
		bCurToggled = true;
		uCursorTicks = 2;
		Sleep(50);
		DrawButton(FACE_IDLE);
	}
}

// Handle mouse wheel cursor
void ProcessMouseWheel(short zDelta) {
	if (zDelta > 0 && uCurBox<9) { //Mousewheel up limited to 9
		uCurBox = uCurBox + 1; SetBox(iCursorX, iCursorY, uCurBox); bGridBackup[iCursorX][iCursorY] = uCurBox;
	}
	else if (zDelta < 0 && uCurBox>0) { //Mousewheel down limited to 0
		uCurBox = uCurBox - 1; SetBox(iCursorX, iCursorY, uCurBox); bGridBackup[iCursorX][iCursorY] = uCurBox;
	}
}

// Handle Play / Pause state
void ToggleGameState() {
	bGamePaused ^= 1;
	if (bGamePaused) {
		CheckMenuItem(ADDR_HMENU, 820, MF_CHECKED);
		KillTimer(ADDR_MAINHWND, 1u);
	}
	else {
		CheckMenuItem(ADDR_HMENU, 820, MF_UNCHECKED);
		SetTimer(ADDR_MAINHWND, 1u, 0x3E8u, 0);
	}
}

// Set difficulty lvl
void ChangeDifficulty(int lvl) {
	iDificulty = lvl;
	CheckMenuItem(ADDR_HMENU, 801, MF_UNCHECKED); 
	CheckMenuItem(ADDR_HMENU, 802, MF_UNCHECKED); 
	CheckMenuItem(ADDR_HMENU, 803, MF_UNCHECKED); 
	CheckMenuItem(ADDR_HMENU, 804, MF_UNCHECKED);
	CheckMenuItem(ADDR_HMENU, 801 + lvl, MF_CHECKED);
}

// Several fixes for bad behabibour on mouse click+move, rbutton, etc...
void FixMouseMoveBugs() {
	int sx = 1; int sy = 1;
	for (int i = 0; i < BOARD_SIZE; i++) {
		//dirty fix for mouse stuff (bug on mouse pressed on white / 9 cells, due to lazyness on stepbox hook, dis works)
		if (GetBox(sx, sy) == 15) //Box is default
			SetBox(sx, sy, 0);
		if (GetBox(sx, sy) == 14) //flag blk on rmouse
			if (bGridInitial[sx][sy])
				SetBox(sx, sy, bGridSolved[sx][sy]);
			else
				SetBox(sx, sy, 0);
		if (GetBox(sx, sy) == 10 && (bGamePaused || bGameEnded)) //Cursor stuck when game finished or paused
			SetBox(sx, sy, uCurBox);
		if (GetBox(sx, sy) == 13) //Question blk on 9's bug
			SetBox(sx, sy, 9);
		sx++; if (sx == 10) { sx = 1; sy++; } //u'll see this often, iterating 9x9 matrix
	}
}

// Our main thread entry point
DWORD GameMainThread() {
	//start random number generator
	srand(unsigned(time(0)));
	//w8 until window is created & install wndproc
	while (!IsWindow(ADDR_MAINHWND)) { Sleep(1); }
	wndproc_install(ADDR_MAINHWND);
	//check dificulty level by default (GG EASY)
	CheckMenuItem(ADDR_HMENU, 801, MF_CHECKED);
	// The logic loop
	while (true) {
		//Some sleep to make cpu happy (and handle our shitty logic)
		Sleep(30);
		//Get actual selection cursor data
		if (!bCurToggled)
			uCurBox = GetBox(iCursorX, iCursorY);
		FixMouseMoveBugs();
		// pause key event P
		if (GetAsyncKeyState(0x50)) {
			ToggleGameState(); 
			Sleep(300);
		}
		//Cursor Task
		uCursorTicks++;
		//Task to manage cursor, counter and handle game finish
		if (uCursorTicks > 5 && !bGameEnded && !bGamePaused) {
			//Change cursor state to show the white cell
			if (!bCurToggled) {
				bCurToggled = true; uCursorTicks = 0;
				SetBox(iCursorX, iCursorY, 10); //Set cell cursor white state
			}
			else {
				bCurToggled = false; uCursorTicks = 0;
				SetBox(iCursorX, iCursorY, uCurBox); //Set cell cursor normal state
				//Now cell is on number view so lets perform some game tasks here
				int iNum = UpdateGameCounter();
				CheckGameFinished(iNum);
				ProcessGameCommands();
			}
		} // end cursor Task
		uKeysTicks++;
		//Task for input keys
		if (uKeysTicks > 6) {
			uKeysTicks = 0;
			//game is on top and running
			if (bHasFocus && !bGameEnded && !bGamePaused)  {
				//Process keys
				ProcessMoveCursor(VK_DOWN,	ORIENTATION_VERTICAL,	DIRECTION_BACKWARD);
				ProcessMoveCursor(VK_RIGHT, ORIENTATION_HORIZONTAL, DIRECTION_BACKWARD);
				ProcessMoveCursor(VK_UP,	ORIENTATION_VERTICAL,	DIRECTION_FORWARD);
				ProcessMoveCursor(VK_LEFT,	ORIENTATION_HORIZONTAL, DIRECTION_FORWARD);
				//Process sudoku COMMAND keys
				CheckSudokuKey(VK_RETURN,	0);
				CheckSudokuKey(VK_BACK,		0);
				int iProcessedKey = 0;
				// Ckeck 0-9 keys
				while (iProcessedKey <= 9) {
					CheckSudokuKey(0x30 + iProcessedKey, iProcessedKey); iProcessedKey++;
				}
				iProcessedKey = 0;
				// Ckeck NUMPAD 0-9 keys
				while (iProcessedKey <= 9) {
					CheckSudokuKey(0x60 + iProcessedKey, iProcessedKey); iProcessedKey++;
				}
			}
		} // end task for input keys
	}
	return 0;
}

// Replace menu from our resoruce
void* LoadMenuWAddr = NULL;
#define HOOK_LOADMENUW 1
HMENU WINAPI LoadMenuWHooked(
	_In_opt_ HINSTANCE hInstance,
	_In_     LPCWSTR   lpMenuName
	) {
	Restore(LoadMenuWAddr, HOOK_LOADMENUW);
	//HMENU hRes = LoadMenuW(hInstance, lpMenuName);
	// Load our custom menu on default HMENU
	HMENU hRes = LoadMenuW(g_hModule, MAKEINTRESOURCEW(IDR_MENU1));
	Redirect(LoadMenuWAddr, LoadMenuWHooked, HOOK_LOADMENUW);
	return hRes;
}

// Dll entry point
BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved) {
	switch (ul_reason_for_call) {
	case DLL_PROCESS_ATTACH: {
		//save it
		g_hModule = hModule;
		//load all winmm stuff
		HookWINMMLib();
		//lets hook
		OrigStepSquare = (StepSquare_t)DetourFunction((BYTE*)(ADDR_STEPSQUARE), (BYTE*)myStepSquare);
		OrigFindBitmap = (FindBitmap_t)DetourFunction((BYTE*)(ADDR_FINDBITMAP), (BYTE*)myFindBitmap);
		OrigStartGame = (StartGame_t)DetourFunction((BYTE*)(ADDR_STARTGAME), (BYTE*)myStartGame);
		OrigReadPreferences = (ReadPreferences_t)DetourFunction((BYTE*)(ADDR_RPREFS), (BYTE*)myReadPreferences);
		//lets redirect other needed calls
		LoadMenuWAddr = GetProcAddress(GetModuleHandle("user32.dll"), "LoadMenuW");
		Redirect(LoadMenuWAddr, LoadMenuWHooked, HOOK_LOADMENUW);
		//start random number generator
		srand(unsigned(time(0)));
		//Create thread of game logic
		g_GameMainHandle = CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)GameMainThread, NULL, NULL, NULL);
	}
	break;
	case DLL_THREAD_ATTACH:
		DisableThreadLibraryCalls(hModule);
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

/*/////////////////////////////////////////////////////////////////////////////////////

							  ╔╗ ╦ ╦  ╔═╗╔═╗╔╗ ╦  ╔═╗╦╔═╔═╗
							  ╠╩╗╚╦╝  ╠═╝╠═╣╠╩╗║  ║ ║╠╩╗║ ║
							  ╚═╝ ╩   ╩  ╩ ╩╚═╝╩═╝╚═╝╩ ╩╚═╝
								 Pabloko - pbl@0x911.ml
			 QQwing: Copyright © Stephen Ostermiller 2005-2014 GNU License
							Microsoft Detours® 1.5 Express

/////////////////////////////////////////////////////////////////////////////////////*/