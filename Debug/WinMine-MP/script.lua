--========================================================================================
--[[======================================================================================

	██╗    ██╗██╗███╗   ██╗███╗   ███╗██╗███╗   ██╗███████╗   ███████╗██╗   ██╗██████╗ 
	██║    ██║██║████╗  ██║████╗ ████║██║████╗  ██║██╔════╝   ██╔════╝██║   ██║██╔══██╗
	██║ █╗ ██║██║██╔██╗ ██║██╔████╔██║██║██╔██╗ ██║█████╗     ███████╗██║   ██║██████╔╝
	██║███╗██║██║██║╚██╗██║██║╚██╔╝██║██║██║╚██╗██║██╔══╝     ╚════██║╚██╗ ██╔╝██╔══██╗
	╚███╔███╔╝██║██║ ╚████║██║ ╚═╝ ██║██║██║ ╚████║███████╗██╗███████║ ╚████╔╝ ██║  ██║
	 ╚══╝╚══╝ ╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝╚═╝╚═╝  ╚═══╝╚══════╝╚═╝╚══════╝  ╚═══╝  ╚═╝  ╚═╝
																					   
							╔╗ ╦ ╦  ╔═╗╔═╗╔╗ ╦  ╔═╗╦╔═╔═╗
							╠╩╗╚╦╝  ╠═╝╠═╣╠╩╗║  ║ ║╠╩╗║ ║
							╚═╝ ╩   ╩  ╩ ╩╚═╝╩═╝╚═╝╩ ╩╚═╝

========================================================================================]]
--========================================================================================

MAX_CLIENTS = 500
LISTEN_PORT = 1337

--Network packets definition
------------------------------------->
ID_NEW_INCOMING_CONNECTION = 19
ID_DISCONNECTION_NOTIFICATION = 21
ID_CONNECTION_LOST = 22
------------------------------------->
ID_NICK_NAME = 135
ID_MAKEMOVE = 136
ID_STARTGAME = 137
ID_ENDGAME = 138
ID_CHAT = 139
ID_COMMAND = 140
ID_ADVERTISE = 141
ID_GONE = 142
------------------------------------->

--Global Definitions
Users={}
REPEAT = -1
--Main (Separate Thread)
main = function()
	Log("##############################################################")
	Log("####                                                      ####")
	Log("####            WINMINE SERVER - EXECUTED                 ####")
	Log("####                                                      ####")
	Log("##############################################################")
	Log("")
	--[[
	On(REPEAT,1000,function() 
		Log("Ayyyy lmaooooo")
	end)
	On(REPEAT,5000,function() 
		Log("#YOLO #SWEG #THUG")
	end)
	]]
	while (true) do
		Sleep(100);
	end
end

--Standard packet handling ==========================================>

NetProcess[ID_NEW_INCOMING_CONNECTION] = function (Sender, bsIn)
	Log(">>> New connection ("..Sender..")");
end

NetProcess[ID_DISCONNECTION_NOTIFICATION] = function (Sender, bsIn)
	--@TODO: handle exit while playing or /retar
	--Remove user from table
	table.remove(Users, Sender)
	--Stream disconnection to all connected users
	local bsOut = NetCreateBitStream(ID_GONE)
	NetWriteBitStream(bsOut, Sender, TUINT8)
	for Key,User in pairs(Users) do
		NetSend(bsOut, Key)
	end
	Log(">>> Disconnected ("..Sender..")")
end

NetProcess[ID_CONNECTION_LOST] = function (Sender, bsIn)
	--@TODO: handle exit while playing or /retar
	table.remove(Users, Sender)
	local bsOut = NetCreateBitStream(ID_GONE)
	NetWriteBitStream(bsOut, Sender, TUINT8)
	for Key,User in pairs(Users) do
		NetSend(bsOut, Key)
	end
	Log(">>> Timed Out ("..Sender..")")
end

--Game Logic

NetProcess[ID_NICK_NAME] = function (Sender, bsIn)
	--Get Nickname
	local sName = NetReadBitStream(bsIn, TSTRING)
	--Stream id and nickname to all connected users
	local bsOut = NetCreateBitStream(ID_NICK_NAME)
	NetWriteBitStream(bsOut, Sender, TUINT8)
	NetWriteBitStream(bsOut, sName, TSTRING)
	for Key,User in pairs(Users) do
		if (Key ~= Sender) then
			NetSend(bsOut, Key)
		end
	end
	-- Stream to newly connected user all players connected
	for Key,User in pairs(Users) do
		local bsOut = NetCreateBitStream(ID_NICK_NAME)
		NetWriteBitStream(bsOut, Key, TUINT8)
		NetWriteBitStream(bsOut, User['NickName'], TSTRING)
		NetSend(bsOut, Sender)
	end
	--Save our user to users table with default data
	Users[Sender]={['NickName'] = sName, ['ChatRoom'] = 0, ['Opponent'] = -1, ['Playing'] = false }
	Log("*** Nick choosed: "..sName.." ("..Sender..")")
end

NetProcess[ID_CHAT] = function (Sender, bsIn)
	--Recieve chat text
	local sText = NetReadBitStream(bsIn, TSTRING)
	Log("*** [CHAT] "..Users[Sender]['NickName'].." ("..Sender.."): "..sText)
	--Stream this message to all other connected users in the same ChatRoom
	for Key,User in pairs(Users) do
		local bsOut = NetCreateBitStream(ID_CHAT)
		NetWriteBitStream(bsOut, Sender, TUINT8)
		NetWriteBitStream(bsOut, sText, TSTRING)
		if ( User['ChatRoom'] == Users[Sender]['ChatRoom'] and Key ~= Sender ) then
			NetSend(bsOut, Key)
		end
	end
end

NetProcess[ID_MAKEMOVE] = function (Sender, bsIn)
	--Read column and row pos
	local uCol = NetReadBitStream(bsIn, TUINT8)
	local uRow = NetReadBitStream(bsIn, TUINT8)
	--Send to the opponent
	local bsOut = NetCreateBitStream(ID_MAKEMOVE)
	NetWriteBitStream(bsOut, uCol, TUINT8)
	NetSend(bsOut, Users[Sender]['Opponent'])
end

NetProcess[ID_ENDGAME] = function (Sender, bsIn)
	--Read match state
	local bWin = NetReadBitStream(bsIn, TBOOL)
	--Broadcast to opponent and self
	local bsOut1 = NetCreateBitStream(ID_ENDGAME)
	NetWriteBitStream(bsOut1, not bWin, TBOOL)
	NetSend(bsOut1, Users[Sender]['Opponent'])
	local bsOut2 = NetCreateBitStream(ID_ENDGAME)
	NetWriteBitStream(bsOut2, bWin, TBOOL)
	NetSend(bsOut2, Sender)
	--Cleanup vars
	Users[Users[Sender]['Opponent']]['ChatRoom']=0;
	Users[Users[Sender]['Opponent']]['Opponent']=-1;
	Users[Sender]['ChatRoom']=0;
	Users[Sender]['Opponent']=-1;
end

NetProcess[ID_COMMAND] = function (Sender, bsIn)
	--Recieve command text
	local sText = NetReadBitStream(bsIn, TSTRING)
	--Split command by space delimiter
	local sCommand = DelimitedStringToTable(sText," ")
	--Compare lowercase command
	if (string.lower(sCommand[1])=="/retar") then
		--Has 2 arguments?
		if (#sCommand ~= 2) then
			Broadcast("/retar [id]", Sender)
		else
			--Is number
			local oid = tonumber(sCommand[2])
			if (oid~=nil) then
				--User is online?
				if (Users[oid]~=nil) then
					--User is free?
					if (Users[oid]['Opponent'] == -1) then
						Users[Sender]['Opponent'] = oid
						Users[oid]['Opponent'] = Sender
						Broadcast("*** "..Users[oid]['NickName'].." ("..oid..") ha sido retado /cancelar", Sender)
						Broadcast("*** "..Users[Sender]['NickName'].." ("..Sender..") te ha retado /aceptar o /cancelar", oid)
					else
						Broadcast("*** Usuario retado o jugando en este momento", Sender)
					end
				else
					Broadcast("*** ID de usuario incorrecto", Sender)
				end
			else
				Broadcast("*** ID de usuario incorrecto", Sender)
			end
		end
	end
	
	--Check /cancelar
	if (string.lower(sCommand[1])=="/cancelar") then
		--Sender have opponent?
		if (Users[Sender]['Opponent'] > -1) then
			--Opponent have sender as opponent?
			if (Users[Users[Sender]['Opponent']]['Opponent'] == Sender) then
				Broadcast("*** ".. Users[Sender]['NickName'].." ("..Sender..") Ha cancelado el reto", Users[Sender]['Opponent'])
				Users[Users[Sender]['Opponent']]['Opponent']=-1;
			end
			Users[Sender]['Opponent']=-1;
			Broadcast("*** Retos cancelados", Sender)
		end
	end
	
	--Check /aceptar
	if (string.lower(sCommand[1])=="/aceptar") then
		--Sender have opponent?
		if (Users[Sender]['Opponent'] > -1) then
			--Opponent have sender as oppoent
			if (Users[Users[Sender]['Opponent']]['Opponent'] == Sender) then
				Broadcast("*** Joining the game...", Sender)
				Broadcast("*** Joining the game...", Users[Sender]['Opponent'])
				local bsOut1 = NetCreateBitStream(ID_STARTGAME)
				NetWriteBitStream(bsOut1, Sender, TUINT8)
				NetWriteBitStream(bsOut1, 0, TBOOL)
				NetSend(bsOut1, Users[Sender]['Opponent'])
				local bsOut2 = NetCreateBitStream(ID_STARTGAME)
				NetWriteBitStream(bsOut2, Users[Sender]['Opponent'], TUINT8)
				NetWriteBitStream(bsOut2, 1, TBOOL)
				NetSend(bsOut2, Sender)
				--Change room
				Users[Sender]['ChatRoom'] = Sender + 100
				Users[Users[Sender]['Opponent']]['ChatRoom'] = Sender + 100
			end
		end
	end
	
	--Check /join
	if (string.lower(sCommand[1])=="/join") then
		--Has 2 arguments?
		if (#sCommand ~= 2) then
			Broadcast("/join [0-99]", Sender)
		else
			--Is number
			local oid = tonumber(sCommand[2])
			if (oid ~= nil) then
				if ( oid >= 0 and oid < 100 ) then
				Users[Sender]['ChatRoom'] = Sender
				end
			end
		end
	end
	
	--Check /pm
	if (string.lower(sCommand[1])=="/pm") then
		--Has 2 arguments?
		if (#sCommand ~= 3) then
			Broadcast("/pm [id] [msg]", Sender)
		else
			local oid = tonumber(sCommand[2])
			if (oid ~= nil) then
				if ( Users[oid] ~= nil ) then
					sCommand[1]=''; sCommand[2]='';
					local sText = table.concat(sCommand, " ")
					Broadcast("[PM] "..Users[Sender]['NickName'].." ("..Sender.."):"..sText ,oid)
				end
			end
		end
	end
	
	--Check /users
	if (string.lower(sCommand[1])=="/users") then
		Broadcast("=== User list ===", Sender)
		for Key,User in pairs(Users) do
			if (Key ~= Sender) then
				Broadcast(User['NickName'].." ("..Key..")", Sender)
			end
		end
		Broadcast("=== User list ===", Sender)
	end
	
end

-- Cmd Commds =======================================================>

onCmd = function (szCmd)
	--Just send the text to all players
	for Key,User in pairs(Users) do
		Broadcast(szCmd, Key)
	end
end

-- Helper functions =================================================>

function Broadcast(sText, To)
	local bsOut = NetCreateBitStream(ID_ADVERTISE)
	NetWriteBitStream(bsOut, sText, TSTRING)
	NetSend(bsOut, To)
end

function DelimitedStringToTable(s, d)
        if not d or #d < 1 then return nil end
        local tbl = {};
        local sa = s;
        local sD = "";
        local nP = string.find(sa, d, 1, true)
        while nP do
                sD = string.sub(sa, 1, nP-1)
                table.insert(tbl, #tbl+1, sD)
                sa = string.sub(sa, nP+1, -1)
                nP = string.find(sa, d, 1, true)
        end
        if sa ~= "" then table.insert(tbl, #tbl+1, sa) end
        return tbl;
end


