/*/////////////////////////////////////////////////////////////////////////////////////



███╗   ███╗ ██████╗ ██████╗        ██╗  ██╗ ██████╗ ██╗  ██╗
████╗ ████║██╔═══██╗██╔══██╗      ╗██║ ██╔╝██╔════╝╗██║ ██╔╝
██╔████╔██║██║   ██║██║  ██║█████╗║█████╔╝ ██║██╗  ║█████╔╝ 
██║╚██╔╝██║██║   ██║██║  ██║╚════╝║██╔═██╗ ██║══╝  ║██╔═██╗ 
██║ ╚═╝ ██║╚██████╔╝██████╔╝      ╝██║  ██╗╚██████╗╝██║  ██╗
╚═╝     ╚═╝ ╚═════╝ ╚═════╝       ═╝  ╚═╝ ╚ ╚═════╝═╝  ╚═╝ ╚
╔═╗╔═╗╦═╗  ╦ ╦╦╔╗╔╔╦╗╦╔╗╔╔═╗  ╔╗ ╦ ╦  ╔═╔═╗╔═╗
╠╣ ║ ║╠╦╝  ║║║║║║║║║║║║║║║╣   ╠╩╗╚╦╝  ╠═╠═╣║ ╗
╚  ╚═╝╩╚═  ╚╩╝╩╝╚╝╩ ╩╩╝╚╝╚═╝  ╚═╝ ╩   ╩ ╩ ╩╚═╝


/////////////////////////////////////////////////////////////////////////////////////*/
#define WIN32_LEAN_AND_MEAN 
// Is build with hacks? (comment for release)
#define HACK_BUILD

//Includes frst
#include <SDKDDKVer.h>
#include <windows.h>
#include <stdio.h>
//#include <stdint.h>
#include <Psapi.h>
#include "../Vendor/Detours15e/detours.h"
#include "../Vendor/Winmm/winmm.hpp"
#include <string.h>
#include "resource.h"
#include <time.h>
#include <CommCtrl.h>
#include <tchar.h>
#include <Shellapi.h>

//Add some libs
#pragma comment (lib, "ws2_32.lib" )
#pragma comment (lib, "psapi" )
#pragma comment (lib, "../Libs/detours.lib")
#pragma comment (lib, "Shell32.lib" )
#pragma comment (lib, "gdiplus.lib" )
#pragma comment (lib, "ComCtl32.lib")

//Include a common controls manifest
#pragma comment(linker, \
  "\"/manifestdependency:type='Win32' "\
  "name='Microsoft.Windows.Common-Controls' "\
  "version='6.0.0.0' "\
  "processorArchitecture='*' "\
  "publicKeyToken='6595b64144ccf1df' "\
  "language='*'\"")

typedef unsigned char       uint8_t;
typedef unsigned short      uint16_t;
typedef unsigned __int32    uint32_t;
typedef signed char         int8_t;
typedef signed short        int16_t;
typedef __int32				int32_t;

// Drawing constants
#define CELL_SIZE 16
#define HEADER_MARGIN 54
#define LEFT_MARGIN 11

// Button faces
#define FACE_IDLE	0
#define FACE_WOT	1
#define FACE_REKT	2
#define FACE_WIN	3

RECT rct;

/* WINMINE'S ADDRS AT VERSION 5.1.2600.0
Language:			1033/1200
CompanyName:        Microsoft Corporation
FileDescription:    Entertainment Pack Minesweeper Game
FileVersion:        5.1.2600.0 (xpclient.010817-1148)
InternalName:       winmine
OriginalFilename:   WINMINE.EXE
ProductName:        Microsoft® Windows® Operating System
ProductVersion:     5.1.2600.0 */

#define ADDR_MAINHWND	*(HWND*)	0x1005B24
#define ADDR_HMENU		*(HMENU*)	0x1005A94
#define ADDR_LEFTCOUNT	*(int*)		0x1005194
#define ADDR_RAWMATRIX				0x1005340
#define ADDR_DISPLAYBLK				0x1002646
#define ADDR_ISCOLOR	*(bool*)	0x10056C8
#define ADDR_BLKBITMAP	*(HGLOBAL*)	0x1005958
#define ADDR_EAUDIO		*(int*)		0x10056B8
#define ADDR_GRIDX1		*(int*)		0x1005334
#define ADDR_GRIDX2		*(int*)		0x1005338
#define ADDR_GRIDY1		*(int*)		0x10056A8
#define ADDR_GRIDY2		*(int*)		0x10056AC
#define ADDR_DISPLAYLC				0x1002801
#define ADDR_DRAWBTN				0x1002913
#define ADDR_STEPSQUARE				0x1003512
#define ADDR_FINDBITMAP				0x10023CD
#define ADDR_STARTGAME				0x100367A
#define ADDR_RPREFS					0x1002BC2

//-- BUTTON , TEXTBOX , LABEL , LISTBOX --
#define TEXTBOX_01  	 601
#define TEXTBOX_02  	 602

#define LISTBOX_01  	 702

#define LABEL_01		 801  

HWND textbox01, textbox02, listbox01, label01;

//--

#include "../Vendor/RakNet4/RakPeerInterface.h"
#include "../Vendor/RakNet4/MessageIdentifiers.h"
#include "../Vendor/RakNet4/RakString.h"
#include "../Vendor/RakNet4/BitStream.h"

//#define RAKPEER_USER_THREADED 1

#define GAMESTATE_CONNECTING 0
#define GAMESTATE_DISCONNECTED 1
#define GAMESTATE_SETNICK 3
#define GAMESTATE_LOBBY 4
#define GAMESTATE_GAME_WAIT 5
#define GAMESTATE_GAME_TURN 6

//-- RAKNET --

#define MAX_CLIENTS 254
#define SERVER_PORT 1337

RakNet::RakPeerInterface *peer;
RakNet::Packet *packet;

//--

enum GameMessages
{
	ID_NICK_NAME = ID_USER_PACKET_ENUM + 1,
	ID_MAKEMOVE,
	ID_STARTGAME,
	ID_ENDGAME,
	ID_CHAT,
	ID_COMMAND,
	ID_ADVERTISE,
	ID_GONE
};

//-- STRUCTS --

struct stPlayers {
	int iPlayerID;
	char szPlayerName[32];
};

stPlayers g_Players[MAX_CLIENTS];

//--
//-- STATIC DEFINITIONS --

//-- GAME UTILS
char szChatMessage[254];
char buf[512];
char szNickName[32];
char szChatBuffer[125];

uint8_t uGameState = 0;
uint8_t uMakeMove = 0;
//-- CURSOR
int					iCursorX = 1;
int					iCursorY = 1;
int					uCursorTicks = 0;
bool				bCurToggled = false;
uint8_t				uCurBox = 0;
//-- KEY HANDLING
int					uKeysTicks = 0;
//-- GAME
int					iDificulty = 0;
bool				bGameEnded = true;
bool				bGamePaused = false;
bool				bHasFocus = true;
bool				iPlayerTurn = 0;
//-- SUDOKU RELATED
const int*			puzzle;
const int*			solution;
//-- GAME COMMANDS
bool				bHint = false;
bool				bCheck = false;
//-- GAME MATRIX
bool				bGridInitial[9][9];
uint8_t				bGridBackup[9][9];
uint8_t				bGridSolved[9][9];
//-- DEFAULT HOOK
HANDLE				g_GameMainHandle;
HMODULE				g_hModule;
static WNDPROC		orig_wndproc;
static HWND			orig_wnd;
//-- HELPERS --
long long			redirectBackup[100];
char				sstring[1024];
int					x, y;
//--

//-- BRUSHES --
HBRUSH hRed = CreateSolidBrush(RGB(255, 0, 0));
HBRUSH hBlue = CreateSolidBrush(RGB(0, 0, 255));
HBRUSH hGray = CreateSolidBrush(RGB(0xcc, 0xcc, 0xcc));
HBRUSH hBlack = CreateSolidBrush(RGB(0 ,0, 0));
HBRUSH hWhite = CreateSolidBrush(RGB(255, 255, 255));
//--

//-- OUR PROTORTYPES --
void		DrawGameOverlay();
uint8_t		GetBox(int x, int y);
void		SetBox(int x, int y, uint8_t b, bool bDisplay = true);
LRESULT CALLBACK GameWndProc(HWND wnd, UINT umsg, WPARAM wparam, LPARAM lparam);
void		ProcessMoveCursor(int vk_key, bool orientation, bool direction);
DWORD		GameMainThread();
void		HookWINMMLib();
void		DrawButton(int btn);
void		FixMouseMoveBugs();
//--

//-- DEFINITIONS CONTOLS DRAWING GAME STUFS --
BOOL bWinmPaint = false;
BOOL bWinmPaint_NewGame_btn = false;
BOOL bWinmPaint_Time = false;
BOOL bWinmPaint_Led = false;
BOOL bWinmGameOver = false;
BOOL bWinmDrawGrid = false;
BOOL bWinmTrackMouse = false;
BOOL bWinmDisplayButton = false;
//--

//type definitions for direct calls to winmine fn's
typedef int(__stdcall *D_ShowBombs)(char);
typedef int(__stdcall *D_ClickBox)(unsigned int, unsigned int);
typedef int(__stdcall *D_DisplayScreen)(HDC hdc);
typedef int(__stdcall *D_DisplayBlk)(int, int);
typedef void(__stdcall*DrawButton_t)(int);
typedef int(__stdcall* DisplayBombCount_t)();

//-- DEFINITIONS FOR DETOURED WINMINE FUNCTIONS --
int __stdcall hkStepSquare(int unk1, int unk2);
typedef int(__stdcall* StepSquare_t)(int unk1, int unk2);
StepSquare_t OrigStepSquare;

int __stdcall hkClearField();
typedef int(__stdcall *ClearBox_t)();
ClearBox_t OrigClearBox;

HRSRC __stdcall hkFindBitmap(__int16 unk1);
typedef HRSRC(__stdcall* FindBitmap_t)(__int16 unk1);
FindBitmap_t OrigFindBitmap;

int __cdecl hkStartGame();
typedef int(__cdecl* StartGame_t)();
StartGame_t OrigStartGame;

int __stdcall hkReadPreferences();
typedef int(__stdcall* ReadPreferences_t)();
ReadPreferences_t OrigReadPreferences;
 
int __stdcall hkDisplayScreen(HDC hDC);
typedef int(__stdcall *DisplayScreen_t)(HDC hDC);
DisplayScreen_t OrigDisplayScreen;

char __stdcall hkTrackMouse(int a1, int a2);
typedef char(__stdcall *TrackMouse_t)(int a1, int a2);
TrackMouse_t OrigTrackMouse;

DWORD __stdcall hkDrawTime(HDC hdc);
typedef DWORD(__stdcall *DrawTime_t)(HDC hdc);
DrawTime_t OrigDrawTime;

int __stdcall hkDrawLed(HDC hdc, int xDest, int a3);
typedef int(__stdcall *DrawLed_t)(HDC hdc, int xDest, int a3);
DrawLed_t OrigDrawLed;

__int16 __stdcall hkGameOver(int a1);
typedef __int16(__stdcall *GameOver_t)(int a1);
GameOver_t OrigGameOver;

int __stdcall hkDrawGrid(HDC hdc);
typedef int(__stdcall *DrawGrid_t)(HDC hdc);
DrawGrid_t OrigDrawGrid;

int __stdcall hkDisplayButton(int a1);
typedef int(__stdcall *DisplayButton_t)(int a1);
DisplayButton_t OrigDisplayButton;

//--


//-- REDIRECTION HELPERS --
#pragma region REDIRECTIONHELPER
void Redirect(void* source, void* destination, int slot) {
	DWORD dwOld;
	VirtualProtect(source, 5, PAGE_EXECUTE_READWRITE, &dwOld);
	redirectBackup[slot] = *(long long*)source;
	*((unsigned char*)source) = 0xE9;
	*(DWORD*)(((char*)source) + 1) = (DWORD)((DWORD)destination - (DWORD)source) - 5;
}

void Restore(void* source, int slot) {
	*(long long*)source = redirectBackup[slot];
}
#pragma endregion
//--

//-- WNDPROC HELPER --
#pragma region WNDPROC
void wndproc_uninstall(void) {
	if (orig_wnd != NULL)
	{
		SetWindowLong(orig_wnd, GWL_WNDPROC, (LONG)(UINT_PTR)orig_wndproc);
		orig_wnd = NULL;
	}
}
void wndproc_install(HWND wnd) {
	if (orig_wndproc == NULL || wnd != orig_wnd)
	{
		wndproc_uninstall();
		orig_wndproc = (WNDPROC)(UINT_PTR)SetWindowLong(wnd, GWL_WNDPROC, (LONG)(UINT_PTR)GameWndProc);
		orig_wnd = wnd;
	}
}
#pragma endregion
//--

//This will draw a header and sudoku grid
void DrawGameOverlay() {
	//Draw the header and prepare everithing
	RECT windowRect;

	GetClientRect(ADDR_MAINHWND, &windowRect);
	int windowX = windowRect.right;
	int windowY = windowRect.bottom;
	HDC hDC;
	hDC = GetDC(ADDR_MAINHWND);
	HFONT hfontArial;
	HGDIOBJ oldfont;
	RECT rect = { 0, 0, windowX, 15 };
	
	hfontArial = CreateFontA(16, 0, FW_DONTCARE, FW_DONTCARE, FW_DONTCARE,
	FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS,
	CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_DONTCARE, "Arial");

	oldfont = SelectObject(hDC, hfontArial);
	SetTextColor(hDC, RGB(0, 0, 0));
	//DrawTextA(hDC, "MineSweeper- Multiplayer MOD", strlen("MineSweeper- Multiplayer MOD"), &rekt, DT_SINGLELINE | DT_VCENTER | DT_CENTER);
	bool bDidChange_NICK = false;
	bool bDidChange_LOBBY = false;
	//Draw Stuff
	switch (uGameState) {
			
	case GAMESTATE_CONNECTING:
	{
		rect.top = 110;

		oldfont = SelectObject(hDC, hfontArial);
		SetTextColor(hDC, RGB(0, 0, 0));
		DrawTextA(hDC, "Conectando...", strlen("Conectando..."), &rect, DT_SINGLELINE | DT_VCENTER | DT_CENTER);

		GetWindowRect(*(HWND*)0x1005B24, &rct);
		SetWindowPos(*(HWND*)0x1005B24, NULL, rct.left, rct.top, 220, 100, NULL);
		UpdateWindow(*(HWND*)0x1005B24);
	}
		break;
	case GAMESTATE_DISCONNECTED:
	{
		rect.top = 100;
	
		oldfont = SelectObject(hDC, hfontArial);
		SetTextColor(hDC, RGB(0, 0, 0));
		DrawTextA(hDC, "Desconectado...", strlen("Desconectado..."), &rect, DT_SINGLELINE | DT_VCENTER | DT_CENTER);

		GetWindowRect(*(HWND*)0x1005B24, &rct);
		SetWindowPos(*(HWND*)0x1005B24, NULL, rct.left, rct.top, 220, 100, NULL);
		UpdateWindow(*(HWND*)0x1005B24);
	}
		break;
	case GAMESTATE_SETNICK:
		if (!bDidChange_NICK)
		{
			GetWindowRect(*(HWND*)0x1005B24, &rct);
			SetWindowPos(*(HWND*)0x1005B24, NULL, rct.left, rct.top, 220, 100, NULL);
			UpdateWindow(*(HWND*)0x1005B24);
			bDidChange_NICK = true;
		}
		break;
	case GAMESTATE_LOBBY:
	{
		if (!bDidChange_LOBBY)
		{
			ShowWindow(GetDlgItem(*(HWND*)0x1005B24, TEXTBOX_02), SW_HIDE);
			ShowWindow(GetDlgItem(*(HWND*)0x1005B24, LABEL_01), SW_HIDE);
			GetWindowRect(*(HWND*)0x1005B24, &rct);
			SetWindowPos(*(HWND*)0x1005B24, NULL, rct.left, rct.top, 540, 240, NULL);
			UpdateWindow(*(HWND*)0x1005B24);
			GetWindowRect(*(HWND*)0x1005B24, &rct);
			SetWindowPos(GetDlgItem(*(HWND*)0x1005B24, LISTBOX_01), NULL, 10, 10, rct.right - rct.left - 30, rct.bottom - rct.top - 20 - 70, NULL);
			SetWindowPos(GetDlgItem(*(HWND*)0x1005B24, TEXTBOX_01), NULL, 10, rct.bottom - rct.top - 20 - 55, rct.right - rct.left - 28, 22, NULL);
			UpdateWindow(*(HWND*)0x1005B24);
			bDidChange_LOBBY = true;
		}
	}
	break;

	}

	HPEN hLinePen;
	COLORREF qLineColor;
	qLineColor = RGB(0, 0, 0);
	hLinePen = CreatePen(PS_SOLID, 2, qLineColor);
	HPEN hPenOld = (HPEN)SelectObject(hDC, hLinePen);

	//finish this shit
	SelectObject(hDC, hPenOld);
	DeleteObject(hLinePen);
	DeleteObject(hfontArial);
	ReleaseDC(ADDR_MAINHWND, hDC);
}

//obtain a winmine's blk content
//-- OUR PROTORTYPES --
uint8_t GetBox(int x, int y) {
	return *(BYTE*)(ADDR_RAWMATRIX + (x + (32 * y)));
}

//set a winmine's blk content (and display it)
void SetBox(int x, int y, uint8_t b, bool bDisplay) {
	int cx = x; int cy = y;
	y = y << 5; y = y + x;
	*(BYTE*)(ADDR_RAWMATRIX + y) = b;
	//Update blk after set it to display
	D_DisplayBlk DisplayBlk = (D_DisplayBlk)ADDR_DISPLAYBLK;
	if (bDisplay)
		DisplayBlk(cx, cy);
	DrawGameOverlay();
}

//Our hooked wndproc, winmine's msgs will arrive here first, then msgs go to defalt wndproc by default
static LRESULT CALLBACK GameWndProc(HWND wnd, UINT umsg, WPARAM wparam, LPARAM lparam) {
	bool bRet = true;
	switch (umsg) {

	case WM_SETFOCUS:
		bHasFocus = true;
		break;
	case WM_KILLFOCUS:
		bHasFocus = false;
		break;
	case WM_ACTIVATE:
	case WM_NCACTIVATE:
		bHasFocus = true;
		break;
		// let the PAINT msg on default wndproc and then draw our stuff on top
	case WM_PAINT: {
		bHasFocus = true;
		CallWindowProc(orig_wndproc, wnd, umsg, wparam, lparam);
		DrawGameOverlay();
		bRet = false;
	}
	break;

	case WM_CLOSE:
	case WM_DESTROY:
		peer->Shutdown(100);
		TerminateThread(g_GameMainHandle, 0); exit(0); //Kill everithing to prevent zombie process on exit
		break;
	} // end switch
	//return to minesweeper wndproc this msg (or not)
	if (bRet)
		return CallWindowProcW(orig_wndproc, wnd, umsg, wparam, lparam);
	else
		return 0;
}
void SendGameEnd(bool bWin) {
	RakNet::BitStream bsOut;
	bsOut.Write((RakNet::MessageID)ID_ENDGAME);
	bsOut.Write(bWin);
	peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, peer->GetSystemAddressFromIndex(0), false);
}

uint8_t PlayerStepColumn(int xX, int yY) {
	uint8_t ret = -1;
	uint8_t vertical = 0;
	uint8_t value = 0;

	iPlayerTurn = 0;
	if (uGameState == GAMESTATE_GAME_TURN)
		iPlayerTurn = 1;

	if (GetBox(xX, 1) == 0) {
		//iPlayerTurn ^= 1;
		do{
			vertical++;
			if (iPlayerTurn)
				SetBox(xX, vertical, 11);
			else
				SetBox(xX, vertical, 12);
			Sleep(100);
			SetBox(xX, vertical, 0);
			if ((vertical + 1) == 9) break;
			value = GetBox(xX, vertical + 1);
		} while (value == 0);
		if (iPlayerTurn)
			SetBox(xX, vertical, 11);
		else
			SetBox(xX, vertical, 12);

		ret = vertical;
		//Aqui es buen momento para mirar si hay win

		//-- COMPROBACION HORIZONTAL --
		uint8_t temp = 0;
		uint8_t p1Score = 0;
		uint8_t p2Score = 0;
		int dx = 1;
		int dy = 1;

		for (int i = 0; i < (8 * 12); i++){
			temp = GetBox(dx, dy);
			if (temp == 11){
				p1Score++;
				p2Score = 0;
			}
			else if (temp == 12){
				p2Score++;
				p1Score = 0;
			}
			else{
				p1Score = 0;
				p2Score = 0;
			}
			if (p1Score == 4){
				SendGameEnd(true);
				break;
			}
			if (p2Score == 4){
				SendGameEnd(false);
				break;
			}
			dx++; if (dx == 13) { dx = 1; dy++; p1Score = 0; p2Score = 0; }
		}
		//-- COMPROBACION VERTICAL --
		dx = 1;
		dy = 1;
		for (int i = 0; i < (8 * 12); i++)
		{
			temp = GetBox(dx, dy);
			if (temp == 11){
				p1Score++;
				p2Score = 0;
			}
			else if (temp == 12){
				p2Score++;
				p1Score = 0;
			}
			else{
				p1Score = 0;
				p2Score = 0;
			}
			if (p1Score == 4){
				SendGameEnd(true);
				break;
			}
			if (p2Score == 4){
				SendGameEnd(false);
				break;
			}
			dy++; if (dy == 9) { dy = 1; dx++; }
		}

		//-- COMPROBACION EN DIAGONAL --
		dx = 1;
		dy = 8;
		for (int i = 0; i < (8 * 12); i++)
		{
			temp = GetBox(dx, dy);

			if (temp == 11 && GetBox(dx + 1, dy + 1) == 11 && GetBox(dx + 2, dy + 2) == 11 && GetBox(dx + 3, dy + 3) == 11){
				SendGameEnd(true);
				break;
			}
			else if (temp == 12 && GetBox(dx + 1, dy + 1) == 12 && GetBox(dx + 2, dy + 2) == 12 && GetBox(dx + 3, dy + 3) == 12){
				SendGameEnd(false);
				break;
			}
			else if (temp == 11 && GetBox(dx - 1, dy + 1) == 11 && GetBox(dx - 2, dy + 2) == 11 && GetBox(dx - 3, dy + 3) == 11){
				SendGameEnd(true); 
				break;
			}
			else if (temp == 12 && GetBox(dx - 1, dy + 1) == 12 && GetBox(dx + 2, dy + 2) == 12 && GetBox(dx - 3, dy + 3) == 12){
				SendGameEnd(false); 
				break;
			}
			else{
				p1Score = 0;
				p2Score = 0;
			}
			dy++; if (dy == 9) { dy = 1; dx++; }
		}
	}
	DrawGameOverlay();
	return ret;
}


// Hook on blk click
int __stdcall myStepSquare(int xX, int yY) {

	if (uGameState == GAMESTATE_GAME_TURN) {

		uint8_t uRow = PlayerStepColumn(xX, 1);
		if (uRow > 0)
		{
			RakNet::BitStream bsOut;
			bsOut.Write((RakNet::MessageID)ID_MAKEMOVE);
			uint8_t uc = xX;
			bsOut.Write(uc);
			bsOut.Write(uRow);
			peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, peer->GetSystemAddressFromIndex(0), false); // esto no envia...

			uGameState = GAMESTATE_GAME_WAIT;
		}
	}
	return 1;
}

// Replace blk bitmaps
HRSRC __stdcall myFindBitmap(__int16 rid) {
	if (rid == 410) { //ID 410 / 411 Contains blk bitmaps 
		bool isColor = (ADDR_ISCOLOR) ^ 1;
		HRSRC src;
		if (isColor)
			src = FindResourceW(g_hModule, MAKEINTRESOURCEW(IDB_BN_BLK), MAKEINTRESOURCEW(2));
		else
			src = FindResourceW(g_hModule, MAKEINTRESOURCEW(IDB_COLOR_BLK), MAKEINTRESOURCEW(2));
		ADDR_BLKBITMAP = LoadResource(g_hModule, src); //Setting this directly is a trick
		return NULL; //This will prevent default FindResource with old instance
	}
	return OrigFindBitmap(rid);
}

// Game start routine hooked
int __cdecl myStartGame() {
	//original fuction call
	int iRet = OrigStartGame();
	//start timer counter
	*(char*)0x1005164 = 1;
	SetTimer(ADDR_MAINHWND, 1u, 0x3E8u, 0);
	//cleanup boxes
	int dx = 1;
	int dy = 1;
	for (int i = 0; i < (ADDR_GRIDX2 * ADDR_GRIDX1); i++){
		SetBox(dx, dy, 0);
		dx++; if (dx == 13) { dx = 1; dy++; }
	}
	return iRet;
}

// fuck preferences, force 9x9 matrix
int __stdcall myReadPreferences() {
	int iRet = OrigReadPreferences();
	//dis shit smash those values (dirty but k)
	ADDR_GRIDX1 = 8;
	ADDR_GRIDY1 = 8;

	ADDR_GRIDX2 = 12;
	ADDR_GRIDY2 = 12;
	//no audio
	ADDR_EAUDIO = 0;
	return iRet;
}

/*clean game*/
int __stdcall myDisplayScreen(HDC hDC)
{
	if (bWinmPaint)
		return OrigDisplayScreen(hDC);
	else
		return 0;
}

char __stdcall myTrackMouse(int a1, int a2)
{
	if (bWinmTrackMouse)
		return OrigTrackMouse(a1, a2);
	else
		return 0;
}

DWORD __stdcall myDrawTime(HDC hdc)
{
	if (bWinmPaint_Time)
		return OrigDrawTime(hdc);
	else
		return 0;

}

int __stdcall myDrawLed(HDC hdc, int xDest, int a3)
{
	if (bWinmPaint_Led)
		return OrigDrawLed(hdc, xDest, a3);
	else
		return 0;
}

__int16 __stdcall myGameOver(int a1)
{
	if (bWinmGameOver)
		return OrigGameOver(a1);
	else
		return 0;
}

int __stdcall myDrawGrid(HDC hdc)
{
	if (bWinmDrawGrid)
		return OrigDrawGrid(hdc);
	else
		return 0;

}
int __stdcall myDisplayButton(int a1)
{
	if (bWinmDisplayButton)
		return OrigDisplayButton(a1);
	else
		return 0;

}

/*end clean*/

// Change face button
void DrawButton(int btn) {
	DrawButton_t hkDrawButton = (DrawButton_t)ADDR_DRAWBTN;
	hkDrawButton(btn);
}


// Several fixes for bad behabibour on mouse click+move, rbutton, etc...
void FixMouseMoveBugs() {
	int sx = 1; int sy = 1;
	for (int i = 0; i < (ADDR_GRIDX2 * ADDR_GRIDX1); i++) {
		//dirty fix for mouse stuff (bug on mouse pressed on white / 9 cells, due to lazyness on stepbox hook, dis works)
		if (GetBox(sx, sy) == 15) //Box is default
			SetBox(sx, sy, 0);
		if (GetBox(sx, sy) == 14) //flag blk on rmouse
			if (bGridInitial[sx][sy])
				SetBox(sx, sy, bGridSolved[sx][sy]);
			else
				SetBox(sx, sy, 0);
		if (GetBox(sx, sy) == 10 && (bGamePaused || bGameEnded)) //Cursor stuck when game finished or paused
			SetBox(sx, sy, uCurBox);
		if (GetBox(sx, sy) == 13) //Question blk on 9's bug
			SetBox(sx, sy, 9);
		sx++; if (sx == (ADDR_GRIDX1+1)) { sx = 1; sy++; } //u'll see this often, iterating 9x9 matrix
	}
}

void sendToListBox(char * text)
{
	WPARAM idx = SendMessage(GetDlgItem(*(HWND*)0x1005B24, LISTBOX_01), LB_ADDSTRING, 0, (LPARAM)text);
	SendMessage(GetDlgItem(*(HWND*)0x1005B24, LISTBOX_01), LB_SETTOPINDEX, idx, 0);
}

// Our main thread entry point
DWORD GameMainThread() {

	//Initialize players

	for (int c = 0; c < MAX_CLIENTS; c++){
		g_Players[c].iPlayerID = -1;
		sprintf(g_Players[c].szPlayerName, "");
	}

	peer = RakNet::RakPeerInterface::GetInstance();
	RakNet::SocketDescriptor sd;


	/*connect*/
	peer->Startup(1, &sd, 1);
	char str[512];

	sprintf(str, "127.0.0.1");

	/*pasguord*/
	char szKey[7] = { 0x30, 0x74, 0x37, 0x50, 0x36, 0x34, 0x06 };

	for (unsigned int fsBrH = 0, tzyGC = 0; fsBrH < 7; fsBrH++)
	{
		tzyGC = szKey[fsBrH];
		tzyGC ^= fsBrH;
		szKey[fsBrH] = tzyGC;
	}

	peer->Connect(str, SERVER_PORT, szKey, 6);

	//start random number generator
	srand(unsigned(time(0)));
	//w8 until window is created & install wndproc
	while (!IsWindow(ADDR_MAINHWND)) { Sleep(1); }
	wndproc_install(ADDR_MAINHWND);
	DrawGameOverlay();
	sprintf(szNickName, "");
	// The logic loop
	while (true) {
		Sleep(30);

		DrawGameOverlay();

		switch (uGameState) {
		case GAMESTATE_CONNECTING:
			
		break;
		case GAMESTATE_DISCONNECTED:
			
		break;
		case GAMESTATE_SETNICK:
		{
			if (GetAsyncKeyState(VK_RETURN))
				{
					GetWindowText(GetDlgItem(*(HWND*)0x1005B24, TEXTBOX_02), szNickName, MAX_PATH);
					if (strlen(szNickName) > 0 && strlen(szNickName) < 24) {

						RakNet::BitStream bsOut;
						bsOut.Write((RakNet::MessageID)ID_NICK_NAME);
						bsOut.Write(szNickName);
						peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, peer->GetSystemAddressFromIndex(0), false);

						UpdateWindow(*(HWND*)0x1005B24);
						InvalidateRect(*(HWND*)0x1005B24, NULL, true);

						uGameState = GAMESTATE_LOBBY;

						Sleep(100);

						HFONT hfontArial = CreateFontA(14, 0, FW_DONTCARE, FW_DONTCARE, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_DONTCARE, "Arial");
						SendMessage(GetDlgItem(*(HWND*)0x1005B24, LISTBOX_01), WM_SETFONT, WPARAM(hfontArial), TRUE);
						SendMessage(GetDlgItem(*(HWND*)0x1005B24, TEXTBOX_01), WM_SETFONT, WPARAM(hfontArial), TRUE);
						sendToListBox("==== Winmine Multiplayer M0D ====");
					}
					else {
						MessageBox(NULL, "Ivalid nickname", "", MB_OK);
					}
				}
		}
		break;
			
		case GAMESTATE_LOBBY:
		case GAMESTATE_GAME_TURN:
		case GAMESTATE_GAME_WAIT:
		{

			if (GetAsyncKeyState(VK_RETURN))
			{
				GetWindowText(GetDlgItem(*(HWND*)0x1005B24, TEXTBOX_01), szChatBuffer, MAX_PATH);
			
				if (strlen(szChatBuffer) > 0)
				{
					RakNet::BitStream bsOut;

					if (szChatBuffer[0] == '/')
						bsOut.Write((RakNet::MessageID)ID_COMMAND);
					else
						bsOut.Write((RakNet::MessageID)ID_CHAT);
					
					bsOut.Write(szChatBuffer);
					peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, peer->GetSystemAddressFromIndex(0), false);
					
					sprintf(buf, "%s: %s", szNickName, szChatBuffer);
					sendToListBox(buf);
					
					sprintf(szChatBuffer, "");
					sprintf(buf, "");
					Sleep(100);
					SetWindowText(GetDlgItem(*(HWND*)0x1005B24, TEXTBOX_01), "");
				}
			}
		}
		break;
		}


		for (packet = peer->Receive(); packet; peer->DeallocatePacket(packet), packet = peer->Receive())
		{
			switch (packet->data[0])
			{
			
			case ID_CONNECTION_REQUEST_ACCEPTED:
			{
				uGameState = GAMESTATE_SETNICK;
			}
			break;
			case ID_NO_FREE_INCOMING_CONNECTIONS:
			case ID_DISCONNECTION_NOTIFICATION:
			case ID_CONNECTION_LOST:
				uGameState = GAMESTATE_DISCONNECTED;
			break;
			
			case ID_MAKEMOVE:
			{
				uint8_t uCol;
				RakNet::BitStream bsIn(packet->data, packet->length, false);
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
				bsIn.Read(uCol);
				PlayerStepColumn(uCol, 1);
				char buf [20];

				sprintf(buf,"Es tu turno %s!",szNickName);
				SendMessage(GetDlgItem(*(HWND*)0x1005B24, LISTBOX_01), LB_ADDSTRING, 0, (LPARAM)buf);
				uGameState = GAMESTATE_GAME_TURN;
			}
				break;

			case ID_STARTGAME:
			{
				int dx = 1;
				int dy = 1;
				for (int i = 0; i < (ADDR_GRIDX2 * ADDR_GRIDX1); i++){
					SetBox(dx, dy, 0);
					dx++; if (dx == 13) { dx = 1; dy++; }
				}

				uint8_t uOpponentID;
				bool bStart = 0;
				RakNet::BitStream bsIn(packet->data, packet->length, false);
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
				bsIn.Read(uOpponentID);
				bsIn.Read(bStart);
				if (bStart) {
					uGameState = GAMESTATE_GAME_TURN;
					iPlayerTurn = true;
				}
				else {
					uGameState = GAMESTATE_GAME_WAIT;
					iPlayerTurn = false;
				}
				sendToListBox("*** Joined game");

				bWinmPaint = true;
				bWinmPaint_NewGame_btn = true;
				bWinmPaint_Time = true;
				bWinmPaint_Led = true;
				bWinmGameOver = true;
				bWinmDrawGrid = true;
				bWinmTrackMouse = true;
				bWinmDisplayButton = true;

				RECT rct;
				GetWindowRect(*(HWND*)0x1005B24, &rct);
				SetWindowPos(*(HWND*)0x1005B24, NULL, rct.left, rct.top, 540, 240, NULL);
				UpdateWindow(*(HWND*)0x1005B24);
				GetWindowRect(*(HWND*)0x1005B24, &rct);
				SetWindowPos(GetDlgItem(*(HWND*)0x1005B24, LISTBOX_01), NULL, 220, 10, rct.right - rct.left - 230, rct.bottom - rct.top - 90, NULL);
				SetWindowPos(GetDlgItem(*(HWND*)0x1005B24, TEXTBOX_01), NULL, 220, rct.bottom - rct.top - 20 - 55, rct.right - rct.left - 230, 22, NULL);

				UpdateWindow(*(HWND*)0x1005B24);
				myStartGame();


			}
				break;

			case ID_CHAT:
			{
				RakNet::RakString rs;
				uint8_t iSender = -1;
				RakNet::BitStream bsIn(packet->data, packet->length, false);
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
				bsIn.Read(iSender);
				bsIn.Read(rs);
				char buf[254+32];

				sprintf(buf, "%s: %s", g_Players[iSender].szPlayerName, rs.C_String());
				sendToListBox(buf);
			}
				break;
			case ID_NICK_NAME:
			{
				RakNet::RakString rs;
				uint8_t iPid = -1;
				RakNet::BitStream bsIn(packet->data, packet->length, false);
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
				bsIn.Read(iPid);
				bsIn.Read(rs);

				g_Players[iPid].iPlayerID = iPid;
				sprintf(g_Players[iPid].szPlayerName, "%s", rs.C_String());
				char buf[254 + 32];
				sprintf(buf, "*** %s (%d) Joined the lobby", rs.C_String(), iPid);
				sendToListBox(buf);
			}
			break;
			case ID_GONE:
			{
				uint8_t iPid = -1;
				RakNet::BitStream bsIn(packet->data, packet->length, false);
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
				bsIn.Read(iPid);

				char buf[254 + 32];
				sprintf(buf, "*** %s (%d) Exited the game", g_Players[iPid].szPlayerName, iPid);
				sendToListBox(buf);
				//SendMessage(GetDlgItem(*(HWND*)0x1005B24, LISTBOX_01), LB_ADDSTRING, 0, (LPARAM)buf);
				g_Players[iPid].iPlayerID = -1;
				sprintf(g_Players[iPid].szPlayerName,"");
			}
			break;
			case ID_ADVERTISE:
			{
				RakNet::RakString rs;
				RakNet::BitStream bsIn(packet->data, packet->length, false);
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
				bsIn.Read(rs);
				sendToListBox((char*)rs.C_String());
			}
			break;
			case ID_ENDGAME:
			{
				bool bWin = false;
				RakNet::BitStream bsIn(packet->data, packet->length, false);
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
				bsIn.Read(bWin);

				if (bWin)
					MessageBox(NULL, "Congratz", "U WIN", MB_OK);
				else
					MessageBox(NULL, "AYY LMAO REKT", "U LOOSE", MB_OK);

				int dx = 1;
				int dy = 1;
				for (int i = 0; i < (ADDR_GRIDX2 * ADDR_GRIDX1); i++){
					SetBox(dx, dy, 0);
					dx++; if (dx == 13) { dx = 1; dy++; }
				}

				bWinmPaint = false;
				bWinmPaint_NewGame_btn = false;
				bWinmPaint_Time = false;
				bWinmPaint_Led = false;
				bWinmGameOver = false;
				bWinmDrawGrid = false;
				bWinmTrackMouse = false;
				bWinmDisplayButton = false;

				UpdateWindow(*(HWND*)0x1005B24);
				InvalidateRect(*(HWND*)0x1005B24, NULL, true);
				uGameState = GAMESTATE_LOBBY;
			}
			break;
			
			}
		}
		FixMouseMoveBugs();
	}
	RakNet::RakPeerInterface::DestroyInstance(0);
	return 0;
}

// Replace menu from our resoruce
void* LoadMenuWAddr = NULL;
#define HOOK_LOADMENUW 1
HMENU WINAPI LoadMenuWHooked(
	_In_opt_ HINSTANCE hInstance,
	_In_     LPCWSTR   lpMenuName
	) {
	Restore(LoadMenuWAddr, HOOK_LOADMENUW);
	HMENU hRes = LoadMenuW(g_hModule, MAKEINTRESOURCEW(IDR_MENU1));
	Redirect(LoadMenuWAddr, LoadMenuWHooked, HOOK_LOADMENUW);
	return NULL;
}

void* CreateWindowExWAddr = NULL;
#define HOOK_CREATEWINDOWEXW 0
HWND WINAPI CreateWindowExWHooked(
	_In_ DWORD dwExStyle,
	_In_opt_ LPCWSTR lpClassName,
	_In_opt_ LPCWSTR lpWindowName,
	_In_ DWORD dwStyle,
	_In_ int x,
	_In_ int y,
	_In_ int nWidth,
	_In_ int nHeight,
	_In_opt_ HWND hWndParent,
	_In_opt_ HMENU hMenu,
	_In_opt_ HINSTANCE hInstance,
	_In_opt_ LPVOID lpParam
	)
{
	Restore(CreateWindowExWAddr, HOOK_CREATEWINDOWEXW);
	HWND wnd = CreateWindowExW(dwExStyle, lpClassName, lpWindowName, dwStyle, x, y, nWidth, nHeight, hWndParent, hMenu, hInstance, lpParam);
	Redirect(CreateWindowExWAddr, CreateWindowExWHooked, HOOK_CREATEWINDOWEXW);
	
	//textbox chat
	if (!textbox01)
		textbox01 = CreateWindowEx(NULL, "EDIT", NULL, WS_VISIBLE | WS_CHILD | WS_BORDER, 220, 160, 290, 20, wnd, (HMENU)TEXTBOX_01, *(HINSTANCE*)0x1005B30, NULL);
	//textbox nickname
	if (!textbox02)
		textbox02 = CreateWindowEx(NULL, "EDIT", NULL, WS_VISIBLE | WS_CHILD | WS_BORDER, 20, 20, 170, 20, wnd, (HMENU)TEXTBOX_02, *(HINSTANCE*)0x1005B30, NULL);
	//label nickname
	if (!label01)
		label01 = CreateWindowEx(NULL, "Static", "Nick Name:", WS_CHILD | WS_VISIBLE, 20, 5, 80, 15, wnd, (HMENU)LABEL_01, *(HINSTANCE*)0x1005B30, 0);
	//chat/console
	if (!listbox01)
		listbox01 = CreateWindowEx(WS_EX_CLIENTEDGE, WC_LISTBOX, NULL, WS_VSCROLL | WS_CHILD | WS_VISIBLE, 220, 5, 290, 150, wnd, (HMENU)(LISTBOX_01), *(HINSTANCE*)0x1005B30, NULL);

	return wnd;
}

// Dll entry point
BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved) {
	switch (ul_reason_for_call) {
	case DLL_PROCESS_ATTACH: {
		//save it
		g_hModule = hModule;
		//load all winmm stuff
		HookWINMMLib();
		//lets hook
		OrigStepSquare = (StepSquare_t)DetourFunction((BYTE*)(ADDR_STEPSQUARE), (BYTE*)myStepSquare);
		OrigFindBitmap = (FindBitmap_t)DetourFunction((BYTE*)(ADDR_FINDBITMAP), (BYTE*)myFindBitmap);
		OrigStartGame = (StartGame_t)DetourFunction((BYTE*)(ADDR_STARTGAME), (BYTE*)myStartGame);
		OrigReadPreferences = (ReadPreferences_t)DetourFunction((BYTE*)(ADDR_RPREFS), (BYTE*)myReadPreferences);

		OrigDisplayScreen = (DisplayScreen_t)DetourFunction((BYTE*)(0x1002AC3), (BYTE*)myDisplayScreen);
		OrigTrackMouse = (TrackMouse_t)DetourFunction((BYTE*)(0x10031D4), (BYTE*)myTrackMouse);
		OrigDrawTime = (DrawTime_t)DetourFunction((BYTE*)(0x1002825), (BYTE*)myDrawTime);
		OrigDrawLed = (DrawLed_t)DetourFunction((BYTE*)(0x1002752), (BYTE*)myDrawLed);
		OrigGameOver = (GameOver_t)DetourFunction((BYTE*)(0x100347C), (BYTE*)myGameOver);
		OrigDrawGrid = (DrawGrid_t)DetourFunction((BYTE*)(0x10026A7), (BYTE*)myDrawGrid);
		OrigDisplayButton = (DisplayButton_t)DetourFunction((BYTE*)(0x1002913), (BYTE*)myDisplayButton);

		//lets redirect other needed calls
		LoadMenuWAddr = GetProcAddress(GetModuleHandle("user32.dll"), "LoadMenuW");
		Redirect(LoadMenuWAddr, LoadMenuWHooked, HOOK_LOADMENUW);

		CreateWindowExWAddr = GetProcAddress(GetModuleHandle("user32.dll"), "CreateWindowExW");
		Redirect(CreateWindowExWAddr, CreateWindowExWHooked, HOOK_CREATEWINDOWEXW);
		//start random number generator
		srand(unsigned(time(0)));
		//Create thread of game logic
		g_GameMainHandle = CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)GameMainThread, NULL, NULL, NULL);
	}
							 break;
	case DLL_THREAD_ATTACH:
		DisableThreadLibraryCalls(hModule);
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

/*/////////////////////////////////////////////////////////////////////////////////////

╔╗ ╦ ╦  ╔══╔═╗╔═╗ 
╠╩╗╚╦╝  ╠══╠═╣║ ╗ 
╚═╝ ╩   ╩  ╩ ╩╚═╝
Fag - KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK
Microsoft Detours® 1.5 Express

/////////////////////////////////////////////////////////////////////////////////////*/